package ir.adak.cookland.network;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.util.List;

import ir.adak.cookland.pojos.ItemPaper;
import ir.adak.cookland.pojos.ResPapsers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class PaperRequestClass {

    private static PaperRequestClass instance;
    private MutableLiveData<List<ItemPaper>> listPaperLiveData;
    private RequestInterface mRequest;

    private PaperRequestClass() {
        listPaperLiveData = new MutableLiveData<>();
        mRequest = RetroClass.getmRetrofit(RetroClass.Base_Url)
                .create(RequestInterface.class);
    }


    public static PaperRequestClass getInstance() {
        if (instance == null) {
            instance = new PaperRequestClass();
        }
        return instance;
    }


    public void getPapers(){

        Call<ResPapsers> papsersCall=mRequest.getPapers("5cdc71dea1db5457a9c20d90096821a2");
        papsersCall.enqueue(new Callback<ResPapsers>() {
            @Override
            public void onResponse(Call<ResPapsers> call, Response<ResPapsers> response) {

                Log.e(RetroClass.TAG, "onResponse Papers :  "+response.raw().request().url());
                Log.e(RetroClass.TAG, "onResponse Papers :  "+response.message());

                Log.e(TAG, "onResponse size of papers : "+response.body().getData().size());
                listPaperLiveData.setValue(response.body().getData());

            }

            @Override
            public void onFailure(Call<ResPapsers> call, Throwable t) {
                Log.e(TAG, "onFailure papers error: "+t.getMessage() );
            }
        });

    }


    public MutableLiveData<List<ItemPaper>> getListPaperLiveData() {
        return listPaperLiveData;
    }
}
