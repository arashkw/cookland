package ir.adak.cookland.network;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.util.List;

import ir.adak.cookland.pojos.ImagesCat;
import ir.adak.cookland.pojos.ResImages;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecipeRequestClass {

    public static RecipeRequestClass instance;
    private RequestInterface mRequest;
    public MutableLiveData<List<ImagesCat>> listcategoryLiveData;


    private RecipeRequestClass() {
        listcategoryLiveData = new MutableLiveData<>();

    }


    public static RecipeRequestClass getInstance() {

        if (instance == null) {
            instance = new RecipeRequestClass();
        }
        return instance;
    }


    public void getCategory() {
        mRequest = RetroClass.getmRetrofit(RetroClass.Base_Url).create(RequestInterface.class);
        Call<ResImages> categoryCall = mRequest.getImg("3ac593bbae3b724786080bf42404d8f8");
        categoryCall.enqueue(new Callback<ResImages>() {
            @Override
            public void onResponse(Call<ResImages> call, Response<ResImages> response) {
                Log.e(RetroClass.TAG, "onResponse: Request Categroy gallery " + response.raw().request().url());
                Log.e(RetroClass.TAG, "onResponse: Message Categroy gallery " + response.message());

                listcategoryLiveData.setValue(response.body().getmData());
                Log.e(RetroClass.TAG, "onResponse: Size Of List Categroy gallery " + response.body().getmData().size());

            }

            @Override
            public void onFailure(Call<ResImages> call, Throwable t) {
                Log.e(RetroClass.TAG, "onFailure get Category List Categroy gallery : "+t );
            }
        });
    }


    public MutableLiveData<List<ImagesCat>> getListcategoryLiveData() {
        return listcategoryLiveData;
    }
}
