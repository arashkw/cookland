package ir.adak.cookland.network;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orhanobut.hawk.Hawk;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroClass extends Application {

    public static Retrofit mRetrofit;
    private Context context;
    public static String Base_Url = "http://adak-tech.ir/gwn/";
    public static final String BASE_URL2 = "http://dev.adak-tech.ir/cookland/web/";
    public static final String BASE_URL_Test = "https://721a6880-1bf9-47f3-a414-0dfafbb6ee01.mock.pstmn.io/";
    public static String Hash = "67a96b42fedba63453fd3bbb0e70f9ee";
    public static final String TAG = "TRACE";
    public static final String TYPE_FOOD = "food";
    public static final String TYPE_Register = "register";
    public static final String TYPE_Confirm = "confirm";
    public static final String TYPE_Recovery = "recovery";
    public static final String TYPE_Login = "login";
    public static final String TYPE_UserName = "userChange";
    public static SharedPreferences mShared;
    public static SharedPreferences.Editor editor;


    @Override
    public void onCreate() {
        super.onCreate();
        this.context = this;
        mShared = PreferenceManager.getDefaultSharedPreferences(context);
        editor = mShared.edit();
        Hawk.init(context).build();
    }


    public static void SetProfileSh(String str) {
        editor.putString("ProfSh", str).commit();
    }

    public static void setPhoneNumber(String number) {
        editor.putString("PhoneNumber", number).commit();
    }

    public static String getPhoneNumber() {
        return mShared.getString("PhoneNumber", null);
    }


    public static String getProfileSh() {
        return mShared.getString("ProfSh", null);
    }


    public static void setToken(String token) {
        editor.putString("Token", token).commit();
    }

    public static String getToken() {
        return mShared.getString("Token", null);
    }


    public static Retrofit getmRetrofit(String address) {

        Gson gson = new GsonBuilder().setLenient().create();
        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(address)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return mRetrofit;

    }

}
