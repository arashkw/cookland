package ir.adak.cookland.network;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.util.List;

import ir.adak.cookland.pojos.ItemFood;
import ir.adak.cookland.pojos.ResFoodItem;
import ir.adak.cookland.pojos.ResSearchFood;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFoodRequestClass {

    public static SearchFoodRequestClass instance;
    public RequestInterface mRequest;
    public MutableLiveData<List<ItemFood>> videosListLivedata;
    private MutableLiveData<List<ItemFood>> searchFoodResult;


    private SearchFoodRequestClass() {
        mRequest = RetroClass.getmRetrofit(RetroClass.Base_Url).create(RequestInterface.class);
        videosListLivedata = new MutableLiveData<>();
        searchFoodResult = new MutableLiveData<>();
    }


    public static SearchFoodRequestClass getInstance() {
        if (instance == null) {
            instance = new SearchFoodRequestClass();
        }
        return instance;
    }


    public void getVideosList(String hash, String Type, String Cat_Id) {

        Call<ResFoodItem> videoListCall = mRequest.getVideos(hash, Type, Cat_Id);

        videoListCall.enqueue(new Callback<ResFoodItem>() {
            @Override
            public void onResponse(Call<ResFoodItem> call, Response<ResFoodItem> response) {

                Log.e(RetroClass.TAG, "onResponse get Video List: " + response.raw().request().url());
                Log.e(RetroClass.TAG, "onResponse get Video List: " + response.message());


                videosListLivedata.setValue(response.body().getListFoodItem());

            }

            @Override
            public void onFailure(Call<ResFoodItem> call, Throwable t) {
                Log.e(RetroClass.TAG, "onFailure get Video List: " + t);
            }
        });
    }

    public void getSearchFood(String hash, String Type, String Key) {

        Call<ResSearchFood> searchFoodCall = mRequest.getSearchFood(hash, Type, Key);

        searchFoodCall.enqueue(new Callback<ResSearchFood>() {
            @Override
            public void onResponse(Call<ResSearchFood> call, Response<ResSearchFood> response) {

                if (response.body() != null) {
                    Log.e(RetroClass.TAG, "onResponse Search Food message: " + response.message());
                    Log.e(RetroClass.TAG, "onResponse Search Food Url: " + response.raw().request().url());

                    searchFoodResult.setValue(response.body().getmResult());

                }
            }

            @Override
            public void onFailure(Call<ResSearchFood> call, Throwable t) {

                Log.e(RetroClass.TAG, "onFailure Search Food: " + t.getMessage());
            }
        });
    }

    public MutableLiveData<List<ItemFood>> getVideosListLivedata() {
        return videosListLivedata;
    }

    public MutableLiveData<List<ItemFood>> getSearchFood() {
        return searchFoodResult;
    }
}
