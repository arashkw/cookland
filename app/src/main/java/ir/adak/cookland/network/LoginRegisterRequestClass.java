package ir.adak.cookland.network;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import ir.adak.cookland.pojos.BaseResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginRegisterRequestClass {

    public static LoginRegisterRequestClass instance;
    private RequestInterface mRequest;
    public int codeResponse;
    public String tokenUser;
    public String tokenAutoLogin;
    public String resultResponse;
    public Context context;


    public LoginRegisterRequestClass() {
        mRequest = RetroClass.getmRetrofit(RetroClass.Base_Url).create(RequestInterface.class);
    }

    public static LoginRegisterRequestClass getInstance() {

        if (instance == null) {
            instance = new LoginRegisterRequestClass();
        }
        return instance;
    }


    public void loginUser(String phoneNumber) {

        Call<BaseResponse> LoginCall = mRequest.getRegister(RetroClass.Hash, phoneNumber, RetroClass.TYPE_Register);

        LoginCall.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                if (response.body() != null) {
                    if (response.body().getCode() == 200) {
                        Log.e(RetroClass.TAG, "onResponse Register: " + response.raw().request().url());
                        codeResponse = response.body().getCode();
                        Log.e(RetroClass.TAG, "onResponse show result --------: " + codeResponse);
                    } else if (response.body().getCode() == 503) {
                        codeResponse = response.body().getCode();
                        Log.e(RetroClass.TAG, "onResponse show result --------: " + codeResponse);

                    }


                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Log.e(RetroClass.TAG, "onFailure Register: " + t.getMessage());
            }
        });
    }

    public void loginAuto(String phoneNumber, String Token) {

        Call<BaseResponse> LoginCall = mRequest.getLoginAuto(RetroClass.Hash, phoneNumber, RetroClass.TYPE_Login, Token);

        LoginCall.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                if (response.body() != null) {
                    if (response.body().getCode() == 200) {
                        Log.e(RetroClass.TAG, "onResponse Register: " + response.raw().request().url());
                        tokenAutoLogin = response.body().getData().getSuccess();
                        Log.e(RetroClass.TAG, "onResponse show result login auto --------: " + tokenAutoLogin);
                    }


                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Log.e(RetroClass.TAG, "onFailure Register: " + t.getMessage());
            }
        });
    }

    public void ConfirmUser(String phoneNumber, int codeVerify) {

        Call<BaseResponse> LoginCall = mRequest.getConfirm(RetroClass.Hash, phoneNumber, RetroClass.TYPE_Confirm, codeVerify);

        LoginCall.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                if (response.body() != null) {
                    if (response.body().getCode() == 200) {

                        Log.e(RetroClass.TAG, "onResponse Confirm: " + response.raw().request().url().toString());
                        Log.e(RetroClass.TAG, "onResponse show confirm token ********: " + response.body().getData().getSuccess());

                        RetroClass.setToken(response.body().getData().getSuccess());
                        tokenUser = response.body().getData().getSuccess().trim();
                        Log.e(RetroClass.TAG, "onResponse log test string token : " + tokenUser);
//                        tokenUser = response.body().getData().getSuccess();
//                        RetroClass.setToken(tokenUser);

                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Log.e(RetroClass.TAG, "onFailure Register: " + t.getMessage());
            }
        });
    }

    public void recoveryCode(String phoneNumber) {

        Call<BaseResponse> LoginCall = mRequest.getRecovery(RetroClass.Hash, phoneNumber, RetroClass.TYPE_Recovery);

        LoginCall.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                if (response.body() != null) {
                    if (response.body().getCode() == 200) {

                        Log.e(RetroClass.TAG, "onResponse Recover code: " + response.raw().request().url().toString());
                        resultResponse = response.body().getData().getSuccess();
                        Log.e(RetroClass.TAG, "onResponse show result --------: " + resultResponse);

                    } else {
                        Toast.makeText(context, "مشکلی در ارسال دوباره کد", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Log.e(RetroClass.TAG, "onFailure Register: " + t.getMessage());
            }
        });
    }


    public int getCodeResponse() {
        return codeResponse;
    }


    public String getResultResponse() {
        return resultResponse;
    }

    public String getTokenAutoLogin() {
        return tokenAutoLogin;
    }
}
