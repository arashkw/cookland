package ir.adak.cookland.network;

import ir.adak.cookland.pojos.BaseResponse;
import ir.adak.cookland.pojos.CaloriCalculator;
import ir.adak.cookland.pojos.ResCategoryFood;
import ir.adak.cookland.pojos.ResFoodItem;
import ir.adak.cookland.pojos.ResImages;
import ir.adak.cookland.pojos.ResPapsers;
import ir.adak.cookland.pojos.ResSearchFood;
import ir.adak.cookland.pojos.Res_Category;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RequestInterface {


    @GET("api/category")
    Call<Res_Category> getCategoryFood(@Query("hash") String Hash, @Query("type") String Type);

    @GET("api/search")
    Call<ResFoodItem> getAllVideos(@Query("hash") String hash, @Query("type") String Type, @Query("key") String key);

    @GET("api/search")
    Call<ResSearchFood> getSearchFood(@Query("hash") String Hash, @Query("type") String Type, @Query("key") String foodName);

    @GET("api/category")
    Call<Res_Category> getCat(@Query("hash") String hash);

    @GET("api/images")
    Call<ResImages> getImg(@Query("hash") String hash);

    @GET("api/article")
    Call<ResPapsers> getPapers(@Query("hash") String hash);

    @GET(".")
    Call<ResCategoryFood> getCat();

    @GET("api/videos")
    Call<ResFoodItem> getVideos(@Query("hash") String hash, @Query("type") String Type, @Query("category") String category_id);

    //<editor-fold desc="Data AtouSearch">
    @GET("api/speclist")
    Call<CaloriCalculator> getMatrial(@Query("hash") String hash,@Query("type")String type);
    //</editor-fold>

    //<editor-fold desc="Suggested data ">
    @GET("api/speclist")
    Call<ResFoodItem> getFindFoods(@Query("hash") String hash,@Query("type")String type ,@Query("params")String params);
    //</editor-fold>


    @GET("api/auth")
    Call<BaseResponse> getRegister(@Query("hash")String Hash, @Query("msisdn")String Number, @Query("type")String Type);
    @GET("api/auth")
    Call<BaseResponse> getRecovery(@Query("hash")String Hash,@Query("msisdn")String Number,@Query("type")String Type);
    @GET("api/auth")
    Call<BaseResponse> getConfirm(@Query("hash")String Hash,
                                  @Query("msisdn")String Number,
                                  @Query("type")String Type,
                                  @Query("code")int Code);

    @GET("api/auth")
    Call<BaseResponse> getLoginAuto(@Query("hash")String Hash,
                                  @Query("msisdn")String Number,
                                  @Query("type")String Type,
                                   @Query("token")String Token);
    @GET("api/auth")
    Call<BaseResponse> getUsername(@Query("hash")String Hash,
                                  @Query("msisdn")String Number,
                                  @Query("type")String Type,
                                  @Query("token")String Token,
                                   @Query("username")String Name);
}
