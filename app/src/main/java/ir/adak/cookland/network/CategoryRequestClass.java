package ir.adak.cookland.network;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.util.List;

import ir.adak.cookland.pojos.ItemCategory;
import ir.adak.cookland.pojos.ItemFood;
import ir.adak.cookland.pojos.ResFoodItem;
import ir.adak.cookland.pojos.Res_Category;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryRequestClass {

    public static CategoryRequestClass instance;
    public RequestInterface mRequest;
    public MutableLiveData<List<ItemFood>> listSlider;
    public MutableLiveData<List<ItemCategory>> listCategoryFood;

    private CategoryRequestClass() {


        listSlider = new MutableLiveData<>();
        listCategoryFood = new MutableLiveData<>();
    }

    public static CategoryRequestClass getInstance() {

        if (instance == null) {
            instance = new CategoryRequestClass();
        }
        return instance;
    }


    public void getSliderItem() {
        mRequest = RetroClass.getmRetrofit(RetroClass.Base_Url).create(RequestInterface.class);
        Call<ResFoodItem> categoryCall = mRequest.getAllVideos(RetroClass.Hash,RetroClass.TYPE_FOOD,"");
        categoryCall.enqueue(new Callback<ResFoodItem>() {
            @Override
            public void onResponse(Call<ResFoodItem> call, Response<ResFoodItem> response) {
                Log.e(RetroClass.TAG, "onResponse: Request Slider " + response.raw().request().url());
                Log.e(RetroClass.TAG, "onResponse: Message Slider " + response.message());

                listSlider.setValue(response.body().getListFoodItem());
                Log.e(RetroClass.TAG, "onResponse: Size Of List Slider " + response.body().getListFoodItem().size());

            }

            @Override
            public void onFailure(Call<ResFoodItem> call, Throwable t) {
                Log.e(RetroClass.TAG, "onFailure get Category List: "+t );
            }
        });
    }


    public void getCategoryFood() {
        mRequest = RetroClass.getmRetrofit(RetroClass.Base_Url).create(RequestInterface.class);
        Call<Res_Category> categoryFoodCall = mRequest.getCategoryFood(RetroClass.Hash,"food");
        categoryFoodCall.enqueue(new Callback<Res_Category>() {
            @Override
            public void onResponse(Call<Res_Category> call, Response<Res_Category> response) {

                Log.e(RetroClass.TAG, "onResponse: category food " + response.raw().request().url());
                listCategoryFood.setValue(response.body().getmData());
                Log.e(RetroClass.TAG, "onResponse: Size  List of category food " + response.body().getmData().size());

            }

            @Override
            public void onFailure(Call<Res_Category> call, Throwable t) {

                Log.e(RetroClass.TAG, "onFailure get Category List food: "+t.getMessage() );

            }
        });

    }

    public MutableLiveData<List<ItemFood>> getListSlider() {
        return listSlider;
    }

    public MutableLiveData<List<ItemCategory>> getListCategoryFood() {
        return listCategoryFood;
    }
}
