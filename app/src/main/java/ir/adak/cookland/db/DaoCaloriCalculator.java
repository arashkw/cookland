package ir.adak.cookland.db;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import java.util.List;
import ir.adak.cookland.pojos.MaterialDB;
import ir.adak.cookland.pojos.ShoppingDB;

@Dao
public interface DaoCaloriCalculator {


    @Insert
    void insert(List<MaterialDB> list);


    @Query("DELETE FROM table_material")
    void deleteAllNotes();


    @Query("SELECT * FROM table_material")
    LiveData<List<MaterialDB>> getAllListAtuo();


    @Insert
    void insertRecipe(ShoppingDB shoppingDB);


    @Query("DELETE FROM table_shop WHERE id = :recipeId")
    void deleteRecipe(int recipeId );


    @Query("SELECT * FROM table_shop")
    LiveData<List<ShoppingDB>> getListRecepi();

}
