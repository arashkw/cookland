package ir.adak.cookland.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import ir.adak.cookland.pojos.MaterialDB;
import ir.adak.cookland.pojos.ShoppingDB;

@Database(entities = {MaterialDB.class, ShoppingDB.class}, version = 4,exportSchema = false)
public abstract class DBCaloriCalculator extends RoomDatabase {

    private static DBCaloriCalculator instanse;
    public abstract DaoCaloriCalculator daoCalori();

    public static synchronized DBCaloriCalculator getInstance(Context context) {

        if (instanse == null) {
            instanse = Room.databaseBuilder(context.getApplicationContext(),
                    DBCaloriCalculator.class, "calorial_database")
                    .fallbackToDestructiveMigration()
                    .build();
        }

        return instanse;
    }
}
