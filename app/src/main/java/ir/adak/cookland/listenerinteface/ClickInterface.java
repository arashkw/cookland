package ir.adak.cookland.listenerinteface;

import android.content.IntentFilter;

import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;

public interface ClickInterface {

     interface ClickDetailsFood {
        void onClickDetails(int hPosition,int vPostion);
    }
     interface ClickOnMore {
        void onMoreClick(int catId);
    }
     interface SerchFoodItemClick{
        void itemFoodClick(int position);
    }


    interface RecipeItemClick{
         void ItemRecipe(int position);
    }

}
