package ir.adak.cookland.ui.meal;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;


import java.util.ArrayList;
import java.util.List;

import ir.adak.cookland.pojos.ItemFood;

public class BaseObserverMeal extends BaseObservable {


    public List<ItemFood> listM;
    private static final String TAG = "MealActivityCat";

    public BaseObserverMeal() {
        listM = new ArrayList<>();
    }

    @Bindable
    public List<ItemFood> getList() {
        return listM;
    }

    public void setList(List<ItemFood> list) {
        this.listM = list;
        notifyPropertyChanged(BR.list);
    }

//
//    @Override
//    public void notifyPropertyChanged(int fieldId) {
//        super.notifyPropertyChanged(fieldId);
//        Log.i(TAG, "notifyPropertyChanged: " + list.size());
//        for (int i = 0; i < list.size(); i++) {
//            Log.i(TAG, "notifyPropertyChanged: " + i + " " + list.get(i).getExistInCatCount());
//        }
  //  }
}
