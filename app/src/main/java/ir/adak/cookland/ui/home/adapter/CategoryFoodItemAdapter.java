package ir.adak.cookland.ui.home.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.airbnb.lottie.LottieAnimationView;

import java.util.List;

import ir.adak.cookland.R;
import ir.adak.cookland.databinding.RowRecyclerCategoryBinding;
import ir.adak.cookland.listenerinteface.ClickInterface;
import ir.adak.cookland.pojos.ItemCategory;
import ir.adak.cookland.pojos.ItemFood;

public class CategoryFoodItemAdapter extends RecyclerView.Adapter<CategoryFoodItemAdapter.ViewHolder> {


    private RowRecyclerCategoryBinding categoryBinding;
    private List<ItemCategory> categoryFoods;
    private List<ItemFood> itemFoods;
    private Context mContext;
    private ClickInterface.ClickOnMore clickOnMore;
    private ItemCategory itemCategoryFood;
    private ClickInterface.ClickDetailsFood clickDetailsFood;

    public CategoryFoodItemAdapter(ClickInterface.ClickDetailsFood clickDetailsFood, ClickInterface.ClickOnMore clickOnMore) {
        this.clickDetailsFood = clickDetailsFood;
        this.clickOnMore = clickOnMore;


    }

    public void setCategoryFoods(List<ItemCategory> categoryFoods) {
        this.categoryFoods = categoryFoods;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        categoryBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_recycler_category, parent, false);
        return new ViewHolder(categoryBinding, clickOnMore);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        itemCategoryFood = categoryFoods.get(position);
        ItemFoodAdapter itemFoodAdapter = new ItemFoodAdapter(clickDetailsFood, position);
        itemFoods = itemCategoryFood.getItemFoodList();
        holder.nameFood.setText(itemCategoryFood.getTitle());
        itemFoodAdapter.setList(itemFoods);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, true);

        holder.foodRecycler.setAdapter(itemFoodAdapter);
        holder.foodRecycler.setHasFixedSize(true);
        holder.foodRecycler.setNestedScrollingEnabled(false);
        linearLayoutManager.setReverseLayout(true);
        SnapHelper snapHelper=new LinearSnapHelper();
        snapHelper.attachToRecyclerView(holder.foodRecycler);
        holder.foodRecycler.setLayoutManager(linearLayoutManager);
        itemFoodAdapter.notifyDataSetChanged();


    }

    @Override
    public int getItemCount() {
        return categoryFoods.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView nameFood;
        private TextView txtMore;
        private RecyclerView foodRecycler;
        private LottieAnimationView lottieAnimationView;
        private ClickInterface.ClickOnMore clickOnMore;


        public ViewHolder(@NonNull RowRecyclerCategoryBinding categoryBinding, ClickInterface.ClickOnMore clickOnMore) {
            super(categoryBinding.getRoot());
            categoryBinding.executePendingBindings();
            this.clickOnMore = clickOnMore;
            nameFood = categoryBinding.textCategory;
            foodRecycler = categoryBinding.recyclerFood;
            txtMore = categoryBinding.moreAllCategory;
            lottieAnimationView = categoryBinding.lavActionBar;

            categoryBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickOnMore.onMoreClick(getAdapterPosition());
                }
            });

            txtMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickOnMore.onMoreClick(getAdapterPosition());
                }
            });

        }


    }


}
