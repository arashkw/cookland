package ir.adak.cookland.ui.papers.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ir.adak.cookland.R;
import ir.adak.cookland.databinding.RowPaperListBinding;
import ir.adak.cookland.pojos.ItemPaper;

public class PapersAdapter extends RecyclerView.Adapter<PapersAdapter.ViewHolder> {

    private Context mContext;
    private List<ItemPaper> paperList;
    private RowPaperListBinding paperListBinding;
    private ItemClick itemClick;

    public PapersAdapter(Context mContext,ItemClick itemClick) {
        this.mContext = mContext;
        this.itemClick = itemClick;
    }

    public void setPaperList(List<ItemPaper> paperList) {
        this.paperList = paperList;
    }

    @NonNull
    @Override
    public PapersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        paperListBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_paper_list, parent, false);
        return new ViewHolder(paperListBinding,itemClick);
    }

    @Override
    public void onBindViewHolder(@NonNull PapersAdapter.ViewHolder holder, int position) {
        ItemPaper itemPaper = paperList.get(position);

        holder.paperTitle.setText(itemPaper.getTitle());
        String description = itemPaper.getContent().substring(0, 30);

        if (itemPaper.getContent().length() > 30) {

            holder.paperDesc.setText(description + "...");
        } else {
            holder.paperDesc.setText(itemPaper.getContent());
        }

        Glide.with(holder.itemView)
                .load(itemPaper.getImage())
                .into(holder.paperImg);


    }

    @Override
    public int getItemCount() {
        return paperList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView paperImg;
        private TextView paperTitle;
        private TextView paperDesc;
        private ItemClick click;

        public ViewHolder(@NonNull RowPaperListBinding paperListBinding,ItemClick click) {
            super(paperListBinding.getRoot());
            paperListBinding.executePendingBindings();

            this.itemView.setOnClickListener(this);
            this.click=click;
            paperImg = paperListBinding.paperImage;
            paperTitle = paperListBinding.paperTitle;
            paperDesc = paperListBinding.paperDescription;
        }

        @Override
        public void onClick(View view) {
            click.clicListenerOnItem(getAdapterPosition());
        }
    }


    public interface ItemClick{
        void clicListenerOnItem(int position);
    }
}
