package ir.adak.cookland.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import ir.adak.cookland.R;
import ir.adak.cookland.databinding.ActivityLoginBinding;
import ir.adak.cookland.network.RetroClass;
import ir.adak.cookland.ui.home.HomeActivity;

public class LoginActivity extends AppCompatActivity {


    private ActivityLoginBinding loginBinding;
    private String editNumber;
    private boolean checkRecover;
    private LoginRegisterViewModel registerViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUi();
    }


    public void initUi() {
        loginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        registerViewModel = ViewModelProviders.of(this).get(LoginRegisterViewModel.class);
        getWindow().setBackgroundDrawableResource(R.drawable.login);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
                | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Log.e(RetroClass.TAG, "onClick response tokennnnnn: " + RetroClass.getToken());


        if (RetroClass.getToken() != null && RetroClass.getPhoneNumber() != null) {
            loginBinding.loadingLayout.setVisibility(View.VISIBLE);
            registerViewModel.getAutoLogin(RetroClass.getPhoneNumber(), RetroClass.getToken());

//                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
//                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loginBinding.loadingLayout.setVisibility(View.GONE);
                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                    }
                }, 2000);

        } else {

            loginBinding.loadingLayout.setVisibility(View.GONE);
            loginBinding.btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editNumber = loginBinding.phonenumber.getText().toString().trim();
                    RetroClass.setPhoneNumber(editNumber);
                    if (editNumber != null) {
                        loginBinding.loadingLayout.setVisibility(View.VISIBLE);
                        registerViewModel.getLogin(editNumber);
                        String str = String.valueOf(registerViewModel.getCodeResponse());
                        Log.e(RetroClass.TAG, "onClick response code: " + registerViewModel.getCodeResponse());
                        if (str != null) {
                            Log.e(RetroClass.TAG, "onClick response code: " + registerViewModel.getCodeResponse());
                            switch (registerViewModel.getCodeResponse()) {
                                case 200:
                                    checkRecover = false;
                                    break;
                                case 503:
                                    checkRecover = true;
                                    break;
                            }
//                            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
//                                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    loginBinding.loadingLayout.setVisibility(View.GONE);
//                                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                    startActivity(new Intent(LoginActivity.this, ConfirmActivity.class)
                                            .putExtra("RECOVER", checkRecover));
                                }
                            }, 2000);
                        }
                    } else {
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.phonenumber), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    public void getRecover(String phoneNumber) {

    }


}
