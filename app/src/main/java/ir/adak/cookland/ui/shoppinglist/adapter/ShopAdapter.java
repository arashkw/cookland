package ir.adak.cookland.ui.shoppinglist.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ir.adak.cookland.R;
import ir.adak.cookland.databinding.RowRecyclerCaloriBinding;
import ir.adak.cookland.databinding.RowRecyclerListShopBinding;
import ir.adak.cookland.pojos.Material;
import ir.adak.cookland.pojos.ShoppingDB;
import ir.adak.cookland.ui.caloriescalculator.adapter.CaloriCalculaterAdpter;

public class ShopAdapter extends RecyclerView.Adapter<ShopAdapter.ViewHolder> {

    private List<ShoppingDB> arrayList;
    private LayoutInflater layoutInflater;
    private DeleteRecipe deleteRecipe;

    public ShopAdapter(List<ShoppingDB> arrayList, DeleteRecipe deleteRecipe) {
        this.arrayList = arrayList;
        this.deleteRecipe = deleteRecipe;

    }

    @NonNull
    @Override
    public ShopAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());

        }

        RowRecyclerListShopBinding rowRecyclerListShopBinding = DataBindingUtil.inflate(layoutInflater, R.layout.row_recycler_list_shop, parent, false);

        return new ShopAdapter.ViewHolder(rowRecyclerListShopBinding, deleteRecipe);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopAdapter.ViewHolder holder, int position) {
        ShoppingDB material = arrayList.get(position);

        holder.bind(material);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RowRecyclerListShopBinding rowRecyclerListShopBinding;
        DeleteRecipe deleteRecipe;

        public ViewHolder(RowRecyclerListShopBinding rowRecyclerListShopBinding, DeleteRecipe deleteRecipe) {
            super(rowRecyclerListShopBinding.getRoot());
            this.rowRecyclerListShopBinding = rowRecyclerListShopBinding;
            this.deleteRecipe = deleteRecipe;


        }

        private void bind(ShoppingDB material) {
            this.rowRecyclerListShopBinding.setData(material);
            this.rowRecyclerListShopBinding.executePendingBindings();




            rowRecyclerListShopBinding.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean checked = ((CheckBox) v).isChecked();

                    switch (v.getId()){
                        case R.id.delete_button:
                            if (checked){
                                deleteRecipe.deletItem(getAdapterPosition());
                            }
                            else {
                                Toast.makeText(v.getContext(),"ohhhh ",Toast.LENGTH_SHORT).show();
                            }
                    }
                }
            });
        }

    }


    public interface DeleteRecipe {
        void deletItem(int position);
    }

}
