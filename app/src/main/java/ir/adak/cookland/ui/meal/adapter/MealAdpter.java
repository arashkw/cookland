package ir.adak.cookland.ui.meal.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.adak.cookland.R;
import ir.adak.cookland.databinding.RowRecyclerMealListBinding;
import ir.adak.cookland.listenerinteface.ClickInterface;
import ir.adak.cookland.pojos.ItemFood;


public class MealAdpter extends RecyclerView.Adapter<MealAdpter.CustomHolder> {

    private List<ItemFood> arrayList;
    private LayoutInflater layoutInflater;
    private static final String TAG = "MealActivityCat";
    private ClickInterface.SerchFoodItemClick serchFoodItemClick;

    public MealAdpter(List<ItemFood> arrayList, ClickInterface.SerchFoodItemClick serchFoodItemClick) {
        this.arrayList = arrayList;
        this.serchFoodItemClick=serchFoodItemClick;
    }

    @NonNull
    @Override
    public CustomHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());

        }

        RowRecyclerMealListBinding itemUserBinding = DataBindingUtil.inflate(layoutInflater, R.layout.row_recycler_meal_list, parent, false);

        return new CustomHolder(itemUserBinding,serchFoodItemClick);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomHolder holder, int position) {
        Log.i(TAG, "onBindViewHolder: " + arrayList.get(position).getExistInCatCount());
        ItemFood material = arrayList.get(position);
        holder.bind(material);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class CustomHolder extends RecyclerView.ViewHolder {

        RowRecyclerMealListBinding itemUserBinding;
        ClickInterface.SerchFoodItemClick serchFoodItemClick;

        public CustomHolder(RowRecyclerMealListBinding itemUserBinding,ClickInterface.SerchFoodItemClick serchFoodItemClick) {
            super(itemUserBinding.getRoot());
            this.itemUserBinding = itemUserBinding;
            this.serchFoodItemClick=serchFoodItemClick;

            this.itemUserBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    serchFoodItemClick.itemFoodClick(getAdapterPosition());
                }
            });

        }

        private void bind(ItemFood material) {
            this.itemUserBinding.setItem(material);
            this.itemUserBinding.executePendingBindings();

        }
    }


}
