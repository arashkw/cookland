package ir.adak.cookland.ui.shoppinglist;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;


import java.util.ArrayList;
import java.util.List;

import ir.adak.cookland.pojos.ShoppingDB;

public class ObserveRecyclerview extends BaseObservable {

    private List<ShoppingDB> listshop;

    public ObserveRecyclerview() {
        listshop=new ArrayList<>();
    }


    @Bindable
    public List<ShoppingDB> getListshop() {

            return listshop;


    }
        public void setListshop (List < ShoppingDB > listshop) {
            this.listshop = listshop;
            notifyPropertyChanged(BR.listshop);
        }
    }
