package ir.adak.cookland.ui.home.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import ir.adak.cookland.R;
import ir.adak.cookland.databinding.RowImageSliderBinding;
import ir.adak.cookland.network.RetroClass;
import ir.adak.cookland.pojos.ItemFood;

public class SliderItemAdapter extends RecyclerView.Adapter<SliderItemAdapter.ViewHolder> {

    private List<ItemFood> itemSlider;
    private Context mContext;
    private LayoutInflater layoutInflater;
    private RowImageSliderBinding imageSliderBinding;
    private ClickSlider clickSlider;

    public SliderItemAdapter(Context mContext, ClickSlider clickSlider) {
        this.mContext = mContext;
        notifyDataSetChanged();
        this.clickSlider = clickSlider;
        layoutInflater = layoutInflater.from(mContext);
    }

    public void setItemSlider(List<ItemFood> itemFoods) {
        this.itemSlider = itemFoods;
        Log.e(RetroClass.TAG, "setItemSlider: Adapter Slider " + itemSlider.size());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        imageSliderBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_image_slider, parent, false);
        return new ViewHolder(imageSliderBinding, clickSlider);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        ItemFood item = itemSlider.get(position);
        holder.textView.setText(item.getTitle());

        Glide.with(holder.imageView)
                .load("http://adak-tech.ir/gwn/uploads/" + itemSlider.get(position).getImage())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.progress_loader.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.progress_loader.setVisibility(View.GONE);
                        return false;
                    }
                })
                .apply(new RequestOptions().placeholder(R.drawable.ic_image_black_24dp).error(R.drawable.ic_image_black_24dp))
                .into(holder.imageView);

    }

    @Override
    public int getItemCount() {
//
        return itemSlider.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private TextView textView;
        private ClickSlider slider;
        private ProgressBar progress_loader;

        public ViewHolder(@NonNull RowImageSliderBinding imageSliderBinding, ClickSlider clickSlider) {
            super(imageSliderBinding.getRoot());
            this.slider = clickSlider;
            imageSliderBinding.executePendingBindings();
            imageView = imageSliderBinding.imageFoodSlider;
            textView = imageSliderBinding.nameFoodSlider;
            progress_loader = imageSliderBinding.progressLoader;

            imageSliderBinding.imageFoodSlider.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    slider.onClickSlider(getAdapterPosition());
                }
            });
        }
    }


    public interface ClickSlider {
        void onClickSlider(int position);
    }

}
