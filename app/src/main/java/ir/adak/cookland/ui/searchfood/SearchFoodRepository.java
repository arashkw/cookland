package ir.adak.cookland.ui.searchfood;

import androidx.lifecycle.MutableLiveData;

import java.util.List;

import ir.adak.cookland.network.SearchFoodRequestClass;
import ir.adak.cookland.pojos.ItemFood;

public class SearchFoodRepository {

    public SearchFoodRequestClass searchFoodRequestClass;
    public MutableLiveData<List<ItemFood>> videosListLivedata;
    public MutableLiveData<List<ItemFood>> itemSearchFood;

    public SearchFoodRepository() {
        searchFoodRequestClass = SearchFoodRequestClass.getInstance();

    }

    public void getvideo(String hash, String type, String cat_id) {
        searchFoodRequestClass.getVideosList(hash, type, cat_id);
        videosListLivedata = searchFoodRequestClass.getVideosListLivedata();
    }

    public void SearchFood(String hash, String type, String key) {

        searchFoodRequestClass.getSearchFood(hash, type, key);
        itemSearchFood = searchFoodRequestClass.getSearchFood();

    }

    public MutableLiveData<List<ItemFood>> getItemSearchFood() {
        return itemSearchFood;
    }

    public MutableLiveData<List<ItemFood>> getVideosListLivedata() {
        return videosListLivedata;
    }
}
