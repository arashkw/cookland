package ir.adak.cookland.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.poovam.pinedittextfield.LinePinField;
import com.poovam.pinedittextfield.PinField;

import org.jetbrains.annotations.NotNull;

import ir.adak.cookland.R;
import ir.adak.cookland.databinding.ActivityVerificationBinding;
import ir.adak.cookland.network.RetroClass;
import ir.adak.cookland.ui.home.HomeActivity;

public class ConfirmActivity extends AppCompatActivity {

    private ActivityVerificationBinding verificationBinding;
    private LoginRegisterViewModel loginRegisterViewModel;
    private TextView sendAgain;
    private Button sendCode;
    private LinePinField linePinField;
    private int codeVerify;
    private String OTPcode;
    private Bundle bundle;
    private Boolean RECOVER;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUi();
    }


    public void initUi() {
        verificationBinding = DataBindingUtil.setContentView(this, R.layout.activity_verification);
        loginRegisterViewModel = ViewModelProviders.of(this).get(LoginRegisterViewModel.class);
        getWindow().setBackgroundDrawableResource(R.drawable.loginphon);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE |
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        RECOVER = getIntent().getBooleanExtra("RECOVER", false);

        Log.e(RetroClass.TAG, "initUi check value: " + RECOVER + " " + RetroClass.getPhoneNumber());


        if (!RECOVER) {
            loginRegisterViewModel.getRecover(RetroClass.getPhoneNumber());
        }

        linePinField = verificationBinding.codeVerify;

        verificationBinding.sendAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginRegisterViewModel.getRecover(RetroClass.getPhoneNumber());
                if (loginRegisterViewModel.getResultResponse().equals("success")) {
                    Toast.makeText(ConfirmActivity.this, "کد تایید دوباره ارسال شد", Toast.LENGTH_SHORT).show();
                }
            }
        });


        linePinField.setOnTextCompleteListener(new PinField.OnTextCompleteListener() {
            @Override
            public boolean onTextComplete(@NotNull String s) {
                OTPcode = s;
//                Toast.makeText(ConfirmActivity.this, ""+codeVerify, Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        verificationBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (OTPcode.length() != 0) {
                    codeVerify=Integer.parseInt(OTPcode);
                    verificationBinding.loadingLayout.setVisibility(View.VISIBLE);

                    loginRegisterViewModel.getConfirm(RetroClass.getPhoneNumber(),codeVerify);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {


                            verificationBinding.loadingLayout.setVisibility(View.GONE);
                            startActivity(new Intent(ConfirmActivity.this, HomeActivity.class));

                        }
                    }, 2000);
                }
            }
        });


    }


}
