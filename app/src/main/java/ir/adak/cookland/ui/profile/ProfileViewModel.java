package ir.adak.cookland.ui.profile;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;

import ir.adak.cookland.ui.profile.dialog.BaseObserveEditBmiDialog;

public class ProfileViewModel extends AndroidViewModel {

    private BaseObserveEditBmiDialog baseobserBmi;



    public ProfileViewModel(Application application) {
        super(application);

        baseobserBmi = new BaseObserveEditBmiDialog();

    }

    public BaseObserveEditBmiDialog getBaseObserverBmi(){
        return baseobserBmi;
    }

}