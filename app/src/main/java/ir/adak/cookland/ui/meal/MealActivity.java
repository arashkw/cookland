package ir.adak.cookland.ui.meal;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import ir.adak.cookland.R;
import ir.adak.cookland.databinding.ActivityMealBinding;
import ir.adak.cookland.listenerinteface.ClickInterface;
import ir.adak.cookland.pojos.ItemFood;
import ir.adak.cookland.ui.meal.adapter.MealAdpter;
import ir.adak.cookland.ui.recipe.RecipeActivity;

public class MealActivity extends AppCompatActivity  {

    private MealViewModel viewModel;
    private ActivityMealBinding binding;
    private BaseObserverMeal baseMeal;
    private String data = "";
    private static final String TAG = "MealActivity";
    private List<ItemFood> mealList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();

    }

    //<editor-fold desc="init for conect exml And Get Items chosen  ">
    public void init() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_meal);
        viewModel = ViewModelProviders.of(MealActivity.this).get(MealViewModel.class);
        data = getIntent().getStringExtra("data");
        Log.i("trace", data);
        if (data != null && data.length() > 0 && data.charAt(data.length() - 1) == ',') {
            Log.i(TAG, "data: " + data);
            baseMeal = viewModel.getBaseMeal();
            binding.setRecycle(baseMeal);
            viewModel.setData(data);

        }

        viewModel.getListLiveData().observe(this, new Observer<List<ItemFood>>() {
            @Override
            public void onChanged(List<ItemFood> itemFoods) {
                mealList=itemFoods;
                Log.i(TAG, "onChanged: "+itemFoods.size());
              //  baseMeal.setList(itemFoods);
            }
        });



    }
    //</editor-fold>

    //<editor-fold desc="Check for CheckBox And Show Item List">
    public void onCheckBoxClicked(View v) {

        boolean checked = ((CheckBox) v).isChecked();

        Log.i(TAG, "onCheckBoxClicked: " + v.getId());
        switch (v.getId()) { //get the id of clicked CheckBox
            case R.id.checkBox_deser:
                Log.i(TAG, "onCheckBoxClicked: " + checked);
                if (checked) {
                    for (int i = 0; i < mealList.size(); i++) {
                        if (mealList.get(i).getAppetizer().equals("1")) {
                            int count = mealList.get(i).getExistInCatCount();
                            count++;
                            mealList.get(i).setExistInCatCount(count);
                        }
                    }
                } else {
                    for (int i = 0; i < mealList.size(); i++) {
                        if (mealList.get(i).getAppetizer().equals("1")) {
                            int count = mealList.get(i).getExistInCatCount();
                            count--;
                            mealList.get(i).setExistInCatCount(count);
                        }
                    }
                }
                break;

            case R.id.checkBox_food:
                if (checked) {
                    for (int i = 0; i < mealList.size(); i++) {
                        if (mealList.get(i).getFood().equals("1")) {
                            int count = mealList.get(i).getExistInCatCount();
                            mealList.get(i).setExistInCatCount(++count);
                        }
                    }
                } else {
                    for (int i = 0; i < mealList.size(); i++) {
                        if (mealList.get(i).getFood().equals("1")) {
                            int count = mealList.get(i).getExistInCatCount();
                            mealList.get(i).setExistInCatCount(--count);
                        }
                    }
                }
                break;

            case R.id.checkBox_break:
                if (checked) {
                    for (int i = 0; i < mealList.size(); i++) {
                        if (mealList.get(i).getBreakfast().equals("1")) {
                            int count = mealList.get(i).getExistInCatCount();
                            mealList.get(i).setExistInCatCount(++count);
                        }
                    }
                } else {
                    for (int i = 0; i < mealList.size(); i++) {
                        if (mealList.get(i).getBreakfast().equals("1")) {
                            int count = mealList.get(i).getExistInCatCount();
                            mealList.get(i).setExistInCatCount(--count);
                        }
                    }
                }
                break;
        }
        baseMeal.setList(getRecyclerViewMealList(mealList));
    }
    //</editor-fold>

    //<editor-fold desc="check According to getExistInCatCount() items for to show and not to show List">
    private List<ItemFood> getRecyclerViewMealList(List<ItemFood> meals) {
        List<ItemFood> recyclerList = new ArrayList<>();
        for (int i = 0; i < meals.size(); i++) {
            if (meals.get(i).getExistInCatCount() > 0)
                recyclerList.add(meals.get(i));
            Log.i(TAG, "getRecyc: "+recyclerList.size());
        }
        return recyclerList;
    }
    //</editor-fold>

    //<editor-fold desc="bind Recyclerview">
    @BindingAdapter("bined:recyclerviewM")
    public static void Recycle(final RecyclerView recyclerView, List<ItemFood> arrayList) {
        MealAdpter adpter = new MealAdpter(arrayList, new ClickInterface.SerchFoodItemClick() {
            @Override
            public void itemFoodClick(int position) {
                Intent intent=new Intent(recyclerView.getContext(), RecipeActivity.class)
                        .putExtra("Item_Pos_Meal",arrayList.get(position));
                recyclerView.getContext().startActivity(intent);
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(adpter);


    }
    //</editor-fold>


}
