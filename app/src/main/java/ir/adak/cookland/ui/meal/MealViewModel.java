package ir.adak.cookland.ui.meal;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ir.adak.cookland.pojos.ItemFood;
import ir.adak.cookland.repository.MealRepository;

public class MealViewModel extends AndroidViewModel {


    private BaseObserverMeal baseMeal;
    private MealRepository mealRepository;
    private LiveData<List<ItemFood>> listLiveData;
    private String data;


    public MealViewModel(@NonNull Application application) {
        super(application);


        mealRepository = MealRepository.getInstance();

        baseMeal = new BaseObserverMeal();
        listLiveData = mealRepository.getMutableLiveData();


    }



    public void setData(String data) {
        mealRepository.getfindFodd(data);

    }

    public BaseObserverMeal getBaseMeal() {
        return baseMeal;
    }

    public LiveData<List<ItemFood>> getListLiveData() {
        return listLiveData;
    }

}
