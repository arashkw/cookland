package ir.adak.cookland.ui.shoppinglist;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ir.adak.cookland.R;
import ir.adak.cookland.databinding.ActivityShopBinding;
import ir.adak.cookland.pojos.Material;
import ir.adak.cookland.pojos.ShoppingDB;
import ir.adak.cookland.ui.caloriescalculator.adapter.CaloriCalculaterAdpter;
import ir.adak.cookland.ui.shoppinglist.adapter.ShopAdapter;

public class ShoppingListActivity extends AppCompatActivity {

    private ActivityShopBinding binding;
    private static ShoppingViewMopdel shoppingViewMopdel;
    private static ObserveRecyclerview obserrecycle;
    private static List<ShoppingDB> list;
    private static List<Integer> pose=new ArrayList<>();
    private static final String TAG = "ShoppingListActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();

        shoppingViewMopdel.getLiveDataGetRecipe().observe(this, new Observer<List<ShoppingDB>>() {
            @Override
            public void onChanged(List<ShoppingDB> shoppingDBS) {

                list = shoppingDBS;
                obserrecycle.setListshop(shoppingDBS);


            }
        });


        binding.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i <pose.size() ; i++) {

                    Log.i(TAG, "Value Pose: "+pose.get(i));

                    shoppingViewMopdel.deleteRecipe(pose.get(i));

                }

            }
        });

    }

    public void init() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_shop);
        shoppingViewMopdel = ViewModelProviders.of(this).get(ShoppingViewMopdel.class);
        obserrecycle = shoppingViewMopdel.getObserveRecyclerview();
        binding.setRecycle(obserrecycle);


    }


    //<editor-fold desc="bind Recyclerview">
    @BindingAdapter({"binde:recycleShop"})
    public static void RecycleBind(final RecyclerView recyclerView, List<ShoppingDB> arrayList) {
        ShopAdapter adpter = new ShopAdapter(arrayList, new ShopAdapter.DeleteRecipe() {
            @Override
            public void deletItem(int position) {

               pose.add(arrayList.get(position).getId());

                Log.i(TAG, "ItemID: "+arrayList.get(position).getId());



            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(adpter);
    }
    //</editor-fold>

}
