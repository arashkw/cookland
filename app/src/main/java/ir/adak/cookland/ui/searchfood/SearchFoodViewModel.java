package ir.adak.cookland.ui.searchfood;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ir.adak.cookland.pojos.ItemFood;
import ir.adak.cookland.pojos.ItemVideoList;

public class SearchFoodViewModel extends AndroidViewModel {

    public LiveData<List<ItemFood>> videoListLiveData;
    public LiveData<List<ItemFood>> itemSearchFood;
    public SearchFoodRepository searchFoodRepository;
    private SearchFoodBaseObservable searchFoodBaseObservable;

    public SearchFoodViewModel(@NonNull Application application) {
        super(application);
        searchFoodRepository = new SearchFoodRepository();
        searchFoodBaseObservable=new SearchFoodBaseObservable();

    }

    public void getVideoList(String hash,String type,String cat_id){
        searchFoodRepository.getvideo(hash,type,cat_id);
        videoListLiveData = searchFoodRepository.getVideosListLivedata();

    }

    public SearchFoodBaseObservable getSearchFoodBaseObservable() {
        return searchFoodBaseObservable;
    }

    public void getSearchFood(String hash, String type, String key){
        searchFoodRepository.SearchFood(hash,type,key);
        itemSearchFood=searchFoodRepository.getItemSearchFood();
    }


    public LiveData<List<ItemFood>> getVideoListLiveData() {
        return videoListLiveData;
    }

    public LiveData<List<ItemFood>> getItemSearchFood() {
        return itemSearchFood;
    }
}
