package ir.adak.cookland.ui.shoppinglist;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ir.adak.cookland.pojos.ShoppingDB;
import ir.adak.cookland.repository.DBCaloriRepository;

public class ShoppingViewMopdel extends AndroidViewModel {

    private LiveData<List<ShoppingDB>> liveDataGetRecipe;
    private DBCaloriRepository dbCaloriRepository;
    private ObserveRecyclerview observeRecyclerview;


    public ShoppingViewMopdel(@NonNull Application application) {
        super(application);

        dbCaloriRepository = new DBCaloriRepository(application);
        observeRecyclerview=new ObserveRecyclerview();
        liveDataGetRecipe = dbCaloriRepository.getListRecipe();

    }

    public ObserveRecyclerview getObserveRecyclerview() {
        return observeRecyclerview;
    }

    public LiveData<List<ShoppingDB>> getLiveDataGetRecipe() {
        return liveDataGetRecipe;
    }

    public void deleteRecipe(int id) {
        dbCaloriRepository.deleteRecipe(id);
    }
}
