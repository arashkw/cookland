package ir.adak.cookland.ui.home.adapter;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import ir.adak.cookland.R;
import ir.adak.cookland.databinding.RowRecyclerFoodBinding;
import ir.adak.cookland.listenerinteface.ClickInterface;
import ir.adak.cookland.network.RetroClass;
import ir.adak.cookland.pojos.ItemFood;

public class ItemFoodAdapter extends RecyclerView.Adapter<ItemFoodAdapter.ViewHolder> {
    public List<ItemFood> list;
    public boolean checkValue;
    public RowRecyclerFoodBinding foodBinding;
    private ClickInterface.ClickDetailsFood detailsFood;
    private ItemFood itemFood;
    private int pos;

    public ItemFoodAdapter(ClickInterface.ClickDetailsFood detailsFood, int position) {
        this.detailsFood = detailsFood;
        this.pos = position;
    }

    public void setList(List<ItemFood> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        foodBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_recycler_food, parent, false);
        return new ViewHolder(foodBinding, detailsFood);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        itemFood = list.get(position);
//        String recipe = itemFood.getDescription();
//        String shortList = recipe.substring(0, 10);
//        if (recipe.length() > 10) {
//            holder.foodDesc.setText(shortList + "...");
//
//        } else {
//            holder.foodDesc.setText(recipe);
//
//        }
        holder.foodName.setText(itemFood.getTitle());
        Glide.with(holder.itemView)
                .load("http://adak-tech.ir/gwn/uploads/" + itemFood.getImage())
                .centerInside()
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.progress_loader.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.progress_loader.setVisibility(View.GONE);
                        return false;
                    }
                })
                .apply(new RequestOptions().placeholder(R.drawable.ic_image_black_24dp).error(R.drawable.ic_image_black_24dp))
                .into(holder.foodImage);


        holder.fav_btn.setBackgroundResource(R.drawable.fav);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView foodImage;
        private TextView foodName;
        private ImageButton fav_btn;
        private TextView foodDesc;
        private ClickInterface.ClickDetailsFood detailsFood;
        private ProgressBar progress_loader;

        public ViewHolder(@NonNull RowRecyclerFoodBinding foodBinding, ClickInterface.ClickDetailsFood detailsFood) {
            super(foodBinding.getRoot());
            foodBinding.executePendingBindings();
            this.detailsFood = detailsFood;
            foodName = foodBinding.nameFood;
            foodImage = foodBinding.imageFood;
            foodDesc = foodBinding.recipyFood;
            fav_btn = foodBinding.btnFavorite;
            progress_loader=foodBinding.progressLoader;

            foodBinding.showDetailsOfFood.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    detailsFood.onClickDetails(getAdapterPosition(), pos);
                }
            });

            foodBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    detailsFood.onClickDetails(getAdapterPosition(), pos);
                }
            });
            fav_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    isRecyclable();
                    if (checkValue == false) {
                        checkValue = true;
                        fav_btn.setBackgroundResource(R.drawable.dofav);
                        Toast.makeText(view.getContext(), R.string.add_fav, Toast.LENGTH_SHORT).show();

                    } else {
                        checkValue = false;
                        fav_btn.setBackgroundResource(R.drawable.fav);
                        Toast.makeText(view.getContext(), R.string.del_fav, Toast.LENGTH_SHORT).show();
                    }
                }
            });


        }
    }


}
