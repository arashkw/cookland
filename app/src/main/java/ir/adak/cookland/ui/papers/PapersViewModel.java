package ir.adak.cookland.ui.papers;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ir.adak.cookland.pojos.ItemPaper;

public class PapersViewModel extends AndroidViewModel {

    private PapersRepositiry repositiry;
    private LiveData<List<ItemPaper>> listPapers;

    public PapersViewModel(@NonNull Application application) {
        super(application);

        repositiry = new PapersRepositiry();
        listPapers = repositiry.getListMutableLiveData();

    }


    public LiveData<List<ItemPaper>> getListPapers() {
        return listPapers;
    }
}
