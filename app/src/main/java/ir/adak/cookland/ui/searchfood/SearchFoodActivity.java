package ir.adak.cookland.ui.searchfood;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;

import java.util.ArrayList;
import java.util.List;

import ir.adak.cookland.R;
import ir.adak.cookland.databinding.ActivitySearchedListBinding;
import ir.adak.cookland.listenerinteface.ClickInterface;
import ir.adak.cookland.network.RetroClass;
import ir.adak.cookland.pojos.ItemFood;
import ir.adak.cookland.ui.recipe.RecipeActivity;
import ir.adak.cookland.ui.searchfood.adapter.SearchFoodAdapter;

public class SearchFoodActivity extends AppCompatActivity implements ClickInterface.SerchFoodItemClick,
        View.OnClickListener {

    private String cat_Id;
    private String cat_Name;
    private Animation slide_down, slide_up;
    private static ActivitySearchedListBinding searchedListBinding;
    private SearchFoodViewModel searchFoodViewModel;
    public List<ItemFood> itemSearchFood = new ArrayList<>();
    private static SearchFoodAdapter searchFoodAdapter;
    private SearchFoodBaseObservable searchFoodBaseObservable;
    private LayoutAnimationController animationi;
    List<ItemFood> itemFood;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        searchedListBinding = DataBindingUtil.setContentView(this, R.layout.activity_searched_list);
        searchFoodViewModel = ViewModelProviders.of(this).get(SearchFoodViewModel.class);


        searchFoodBaseObservable = searchFoodViewModel.getSearchFoodBaseObservable();
        searchedListBinding.setItemFood(searchFoodBaseObservable);
        initUi();


        //<editor-fold desc="Get Video List"
        searchFoodViewModel.getVideoList(RetroClass.Hash, RetroClass.TYPE_FOOD, cat_Id);
        searchedListBinding.foodCategoryName.setText(cat_Name);

        searchFoodViewModel.getVideoListLiveData().observe(this, new Observer<List<ItemFood>>() {
            @Override
            public void onChanged(List<ItemFood> itemFoods) {
                itemFood = itemFoods;
                searchFoodBaseObservable.setItemFoodList(itemFood);
            }
        });
        //</editor-fold>


        //<editor-fold desc="Search Food">
        searchedListBinding.searchFood.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchFoodViewModel.getSearchFood(RetroClass.Hash, RetroClass.TYPE_FOOD, newText);

                searchFoodViewModel.getItemSearchFood().observe(SearchFoodActivity.this, new Observer<List<ItemFood>>() {
                    @Override
                    public void onChanged(List<ItemFood> itemFoods) {

                        searchFoodBaseObservable.setItemFoodList(itemFoods);
                    }

                });
                return false;
            }
        });
        //</editor-fold>


        searchedListBinding.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

    public void initUi() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN |
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        View view = this.getCurrentFocus();

        if(view!=null){
            InputMethodManager imm= (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(),0);
        }

        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            cat_Id = extra.getString("Cat_Id");
            cat_Name = extra.getString("Cat_Name");
        }

        slide_down = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.search_in);
        slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.search_in_layout);

        int res_anim = R.anim.layout_animation_down_to_up;
        animationi = AnimationUtils.loadLayoutAnimation(this, res_anim);
        searchFoodAdapter = new SearchFoodAdapter(this);
        searchFoodAdapter.setVideoLists(new ArrayList<>());
        searchedListBinding.recyclerVideoList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        searchedListBinding.recyclerVideoList.setAdapter(searchFoodAdapter);
        searchedListBinding.recyclerVideoList.setLayoutAnimation(animationi);


    }


    @Override
    public void itemFoodClick(int position) {
        startActivity(new Intent(this, RecipeActivity.class)
                .putExtra("Item_Pos", itemSearchFood.get(position))
        );
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

        }
    }


    @BindingAdapter("binde:recycleFood")
    public static void RecycleBind(final RecyclerView recyclerView, List<ItemFood> arrayList) {
        searchFoodAdapter = new SearchFoodAdapter(new ClickInterface.SerchFoodItemClick() {
            @Override
            public void itemFoodClick(int position) {

                recyclerView.getContext().startActivity(new Intent(recyclerView.getContext(), RecipeActivity.class)
                        .putExtra("Item_Food",
                                arrayList.get(position)));

            }
        });

        searchFoodAdapter.setVideoLists(arrayList);
        searchedListBinding.recyclerVideoList.scheduleLayoutAnimation();
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(searchFoodAdapter);


        //<editor-fold desc="Shimmer Start Animation">
        searchedListBinding.recyclerVideoList.showShimmerAdapter();     // to start showing shimmer
        searchedListBinding.recyclerVideoList.setDemoLayoutManager(ShimmerRecyclerView.LayoutMangerType.LINEAR_VERTICAL);     // to start showing shimmer

        // To stimulate long running work using android.os.Handler
        new Handler().postDelayed((Runnable) () -> {
            searchedListBinding.recyclerVideoList.hideShimmerAdapter(); // to hide shimmer
            searchedListBinding.recyclerVideoList.getActualAdapter(); // to hide shimmer
        }, 3000);
        //</editor-fold>
    }

}
