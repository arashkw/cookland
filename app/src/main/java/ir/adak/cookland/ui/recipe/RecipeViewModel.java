package ir.adak.cookland.ui.recipe;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ir.adak.cookland.pojos.ImagesCat;
import ir.adak.cookland.pojos.ShoppingDB;
import ir.adak.cookland.repository.DBCaloriRepository;

public class RecipeViewModel extends AndroidViewModel {

    public RecipeRepository recipeRepository;
    public LiveData<List<ImagesCat>> listLiveDataCategory;
    private DBCaloriRepository dbCaloriRepository;
    private LiveData<List<ShoppingDB>> listRecipeDatabase;
    private RecipeBaseObservable recipeBaseObservable;


    public RecipeViewModel(@NonNull Application application) {
        super(application);

        recipeRepository = new RecipeRepository();
        recipeBaseObservable=new RecipeBaseObservable();
        dbCaloriRepository = new DBCaloriRepository(application);
        recipeRepository.getListCategory();
        listLiveDataCategory = recipeRepository.getListCategory();
        listRecipeDatabase = dbCaloriRepository.getListRecipe();


    }

    public void getInsertRecipe(ShoppingDB itemList) {
        dbCaloriRepository.insertRecipe(itemList);
    }

    public void getDeleteItem(int itemId) {
        dbCaloriRepository.deleteRecipe(itemId);
    }


    public LiveData<List<ImagesCat>> getListLiveDataCategory() {
        return listLiveDataCategory;
    }

    public LiveData<List<ShoppingDB>> getListRecipeDatabase() {
        return listRecipeDatabase;
    }

    public RecipeBaseObservable getRecipeBaseObservable() {
        return recipeBaseObservable;
    }


}
