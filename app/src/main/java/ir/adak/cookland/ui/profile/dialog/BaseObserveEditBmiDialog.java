package ir.adak.cookland.ui.profile.dialog;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.fragment.app.DialogFragment;

public class BaseObserveEditBmiDialog extends BaseObservable {

    private String Height;
    private String Weight;
    private String Age;
    private String Gender;
    private String bmi;


    @Bindable
    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
        notifyPropertyChanged(BR.height);

    }
    @Bindable
    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
        notifyPropertyChanged(BR.weight);
    }

    @Bindable
    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        this.Age = age;
        notifyPropertyChanged(BR.age);
    }

    @Bindable
    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
        notifyPropertyChanged(BR.gender);
    }

    @Bindable
    public String getBmi() {
        return bmi;
    }

    public void setBmi(String bmi) {
        this.bmi = bmi;
        notifyPropertyChanged(BR.bmi);
    }
}
