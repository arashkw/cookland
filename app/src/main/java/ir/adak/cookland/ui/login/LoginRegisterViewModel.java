package ir.adak.cookland.ui.login;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

public class LoginRegisterViewModel extends AndroidViewModel {

    private LoginRegisterRepository registerRepository;
    private String resultResponse;
    private int codeResponse;
    private String TokenUser;
    private String TokenAutoLogin;

    public LoginRegisterViewModel(@NonNull Application application) {
        super(application);

        registerRepository = new LoginRegisterRepository();

    }

    public void getLogin(String number) {
        registerRepository.getLogin(number);
        codeResponse = registerRepository.getCodeResponse();
    }

    public void getConfirm(String number, int codeVerify) {
        registerRepository.getConfirm(number, codeVerify);
    }

    public void getRecover(String number) {
        registerRepository.getRecovery(number);
        resultResponse = registerRepository.getResultResponse();
    }

    public void getAutoLogin(String number, String token) {
        registerRepository.getAutoLogin(number, token);
        TokenAutoLogin = registerRepository.getTokenAutologin();
    }

    public String getResultResponse() {
        return resultResponse;
    }


    public String getTokenAutoLogin() {
        return TokenAutoLogin;
    }

    public int getCodeResponse() {
        return codeResponse;
    }
}
