package ir.adak.cookland.ui.caloriescalculator.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.adak.cookland.R;
import ir.adak.cookland.databinding.RowRecyclerCaloriBinding;
import ir.adak.cookland.pojos.Material;

public class CaloriCalculaterAdpter extends RecyclerView.Adapter<CaloriCalculaterAdpter.CustomHolder> {

    private List<Material> arrayList;
    private LayoutInflater layoutInflater;
    MaterialEditer materialEditer;

    public CaloriCalculaterAdpter(List<Material> arrayList, MaterialEditer materialEditer) {
        this.arrayList = arrayList;
        this.materialEditer = materialEditer;
    }

    @NonNull
    @Override
    public CustomHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());

        }

        RowRecyclerCaloriBinding itemUserBinding = DataBindingUtil.inflate(layoutInflater, R.layout.row_recycler_calori, parent, false);

        return new CustomHolder(itemUserBinding, materialEditer);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomHolder holder, int position) {
        Material material = arrayList.get(position);

        holder.bind(material);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class CustomHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        RowRecyclerCaloriBinding itemUserBinding;
        MaterialEditer materialEditer;

        public CustomHolder(RowRecyclerCaloriBinding itemUserBinding, MaterialEditer materialEditer) {
            super(itemUserBinding.getRoot());
            this.itemUserBinding = itemUserBinding;
            this.materialEditer = materialEditer;
            this.itemUserBinding.setOnclik(this);


        }

        private void bind(Material material) {
            this.itemUserBinding.setItem(material);
            this.itemUserBinding.executePendingBindings();

        }

        @Override
        public void onClick(View v) {
            materialEditer.deletItem(getAdapterPosition());


        }
    }

    public interface MaterialEditer {
        void deletItem(int position);
    }

}
