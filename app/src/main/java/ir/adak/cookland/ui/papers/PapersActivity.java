package ir.adak.cookland.ui.papers;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ir.adak.cookland.R;
import ir.adak.cookland.databinding.ActivityPapersBinding;
import ir.adak.cookland.pojos.ItemPaper;
import ir.adak.cookland.ui.papers.adapter.PapersAdapter;

public class PapersActivity extends AppCompatActivity implements PapersAdapter.ItemClick {

    private ActivityPapersBinding papersBinding;
    private PapersViewModel papersViewModel;
    private PapersAdapter adapter;
    private List<ItemPaper> itemPaperList;
    private Animation slide_down, slide_up;
    private boolean checkLayout;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        papersBinding = DataBindingUtil.setContentView(this, R.layout.activity_papers);
        papersViewModel = ViewModelProviders.of(this).get(PapersViewModel.class);


        papersViewModel.getListPapers();
        adapter = new PapersAdapter(this, this);
        adapter.setPaperList(new ArrayList<>());
        papersBinding.recyclerPapers.setAdapter(adapter);
        papersBinding.recyclerPapers.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));


        papersViewModel.getListPapers().observe(this, new Observer<List<ItemPaper>>() {
            @Override
            public void onChanged(List<ItemPaper> itemPapers) {
                adapter.setPaperList(itemPapers);
                adapter.notifyDataSetChanged();

                itemPaperList = itemPapers;


            }
        });


        papersBinding.recyclerPapers.showShimmerAdapter();     // to start showing shimmer
        // to start showing shimmer
        // To stimulate long running work using android.os.Handler
        new Handler().postDelayed((Runnable) () -> {
            papersBinding.recyclerPapers.hideShimmerAdapter(); // to hide shimmer
        }, 3000);
    }

    @Override
    public void clicListenerOnItem(int position) {

        slide_down = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.search_in);

        slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.search_in_layout);
        papersBinding.parentlayout.startAnimation(slide_down);
        checkLayout = true;
        papersBinding.parentlayout.setVisibility(View.GONE);


        papersBinding.childLayout.startAnimation(slide_up);
        papersBinding.childLayout.setVisibility(View.VISIBLE);


        showItem(position);

    }


    @Override
    public void onBackPressed() {
        if (checkLayout == false) {
            finish();
        } else {
            papersBinding.parentlayout.startAnimation(slide_up);
            papersBinding.parentlayout.setVisibility(View.VISIBLE);
            checkLayout = false;
            papersBinding.childLayout.startAnimation(slide_down);
            papersBinding.childLayout.setVisibility(View.GONE);
        }
    }

    public void showItem(int i) {
        papersBinding.titlePaper.setText(itemPaperList.get(i).getTitle());
        papersBinding.textPaper.setText(itemPaperList.get(i).getContent());

        Glide.with(this)
                .load(itemPaperList.get(i).getImage())
                .centerCrop()
                .into(papersBinding.imgPaper);


        papersBinding.hideBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                papersBinding.parentlayout.startAnimation(slide_up);
                papersBinding.parentlayout.setVisibility(View.VISIBLE);
                checkLayout = false;
                papersBinding.childLayout.startAnimation(slide_down);
                papersBinding.childLayout.setVisibility(View.GONE);

            }
        });


        papersBinding.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


}
