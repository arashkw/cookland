package ir.adak.cookland.ui.recipe.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ir.adak.cookland.R;
import ir.adak.cookland.databinding.RowGalleryDetailsBinding;
import ir.adak.cookland.pojos.ImagesCat;

public class RecipeGalleryAdapter extends RecyclerView.Adapter<RecipeGalleryAdapter.ViewHolder>  {


    private List<ImagesCat> itemCategoryList;
    private RowGalleryDetailsBinding galleryDetailsBinding;
    private Context mContext;

    public RecipeGalleryAdapter(Context mContext, List<ImagesCat> itemCategoryList) {
        this.mContext = mContext;
        this.itemCategoryList = itemCategoryList;
    }

    public void setItemCategoryList(List<ImagesCat> itemCategoryList) {
        this.itemCategoryList = itemCategoryList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       galleryDetailsBinding=DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.row_gallery_details,parent,false);
        return new ViewHolder(galleryDetailsBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        ImagesCat item=itemCategoryList.get(position);

//        Log.e("trace", "onBindViewHolder sliderItemAdapter new : " +item.getImage() );
        Glide.with(mContext)
                .load(item.getThumb())
                .into(holder.imgGallery);

    }

    @Override
    public int getItemCount() {
        return itemCategoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgGallery;

        public ViewHolder(@NonNull RowGalleryDetailsBinding itemview) {
            super(itemview.getRoot());
            itemview.executePendingBindings();
            imgGallery=itemview.imageGalleryDetails;



        }
    }
}
