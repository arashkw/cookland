package ir.adak.cookland.ui.home;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.util.List;

import ir.adak.cookland.network.CategoryRequestClass;
import ir.adak.cookland.network.RetroClass;
import ir.adak.cookland.pojos.ItemCategory;
import ir.adak.cookland.pojos.ItemFood;

public class CategoryRepository {

    CategoryRequestClass categoryRequestClass;
    public MutableLiveData<List<ItemFood>> listSliderItem;
    public MutableLiveData<List<ItemCategory>> listCategoryFood;

    public CategoryRepository() {
        categoryRequestClass=CategoryRequestClass.getInstance();
        categoryRequestClass.getSliderItem();
        categoryRequestClass.getCategoryFood();

        listSliderItem =categoryRequestClass.getListSlider();
//        Log.e(RetroClass.TAG, "CategoryRepository: Size Of List Category "+listSliderItem.getValue().size() );

        listCategoryFood=categoryRequestClass.getListCategoryFood();

    }

    public MutableLiveData<List<ItemFood>> getListSliderItem() {
        return listSliderItem;
    }

    public MutableLiveData<List<ItemCategory>> getListCategoryFood() {
        return listCategoryFood;
    }
}
