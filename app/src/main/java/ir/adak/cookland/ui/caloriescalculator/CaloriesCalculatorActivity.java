package ir.adak.cookland.ui.caloriescalculator;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import ir.adak.cookland.R;
import ir.adak.cookland.databinding.CaloriescalculatorActivityBinding;
import ir.adak.cookland.databinding.ObserveDialogEditcoleriBinding;
import ir.adak.cookland.pojos.Material;
import ir.adak.cookland.pojos.MaterialDB;
import ir.adak.cookland.pojos.ShoppingDB;
import ir.adak.cookland.ui.caloriescalculator.adapter.CaloriCalculaterAdpter;
import ir.adak.cookland.ui.caloriescalculator.adapter.CustomAdapter;
import ir.adak.cookland.ui.meal.MealActivity;

public class CaloriesCalculatorActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private CaloriescalculatorActivityBinding binding;
    private CaloriesCalculatorViewModel calculatorViewModel;
    private CustomAdapter customAdapter;
    private static BaseObserverCalori baseCalori;
    ObserveDialogEditcoleriBinding editcoleriBinding;
    private List<MaterialDB> atuolList = new ArrayList<>();
    private static List<Material> addtoList = new ArrayList<>();
    private static final String TAG = "CaloriesCalculator";
    private static final String TAG2 = "Calorimeeee";
    private AlertDialog.Builder builder;
    private static double sumcalori = 0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        init();

        //<editor-fold desc="Check Connect wifa And Get Data from server insert to Database" ">
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CaloriesCalculatorActivity.this.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network

            calculatorViewModel.getLiveDataRequest().observe(this, new Observer<List<MaterialDB>>() {
                @Override
                public void onChanged(List<MaterialDB> materials) {
                    Log.i(TAG, "obser request: " + materials.size());
                    calculatorViewModel.deleteDB();
                    calculatorViewModel.insertDB(materials);


                }
            });

        }
        //</editor-fold>

        //<editor-fold desc="Get Data from Database">
        calculatorViewModel.getAllDB().observe(this, new Observer<List<MaterialDB>>() {
            @Override
            public void onChanged(List<MaterialDB> materials) {

                Log.i(TAG, "obser database: material " + materials.size());
                atuolList = materials;
                customAdapter = new CustomAdapter(CaloriesCalculatorActivity.this, atuolList);
                binding.autoCompleteSpinner.setAdapter(customAdapter);

                binding.autoCompleteSpinner.setThreshold(1);
                Log.i(TAG, "obser database: autoarray" + atuolList.size());
                customAdapter.notifyDataSetChanged();

            }
        });
        //</editor-fold>
    }


    //<editor-fold desc="init for conect exml">
    public void init() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        calculatorViewModel = ViewModelProviders.of(this).get(CaloriesCalculatorViewModel.class);
        binding = DataBindingUtil.setContentView(this, R.layout.caloriescalculator_activity);

        baseCalori = calculatorViewModel.getBaseCalori();

        binding.setClick(this);
        binding.setBaseobser(baseCalori);
        binding.autoCompleteSpinner.setOnItemClickListener(this);


    }
    //</editor-fold>

    //<editor-fold desc="bind Recyclerview">
    @BindingAdapter("binde:recycleUser")
    public static void RecycleBind(final RecyclerView recyclerView, List<Material> arrayList) {
        CaloriCalculaterAdpter adpter = new CaloriCalculaterAdpter(arrayList, new CaloriCalculaterAdpter.MaterialEditer() {
            @Override
            public void deletItem(int position) {
                double caloriminess = 0;

                Log.i(TAG2, "deletItem: " + position);
                caloriminess = addtoList.get(position).getCalories();

                addtoList.remove(position);
                baseCalori.setList(addtoList);

                sumcalori = sumcalori - caloriminess;
                baseCalori.setAddmount(sumcalori + "");
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(adpter);

    }
    //</editor-fold>

    //<editor-fold desc="sum Coleries">
    public void AddCalori() {
        double caloris = 0;

        for (int i = 0; i < addtoList.size(); i++) {

            caloris = addtoList.get(i).getCalories();
            Log.i(TAG2, "getBefor: " + caloris + "    " + +i);

        }

        Log.i(TAG2, "get: " + sumcalori);
        sumcalori = caloris + sumcalori;
        Log.i(TAG2, "getAfter: " + sumcalori);

        baseCalori.setAddmount(sumcalori + "");
    }
    //</editor-fold>

    //<editor-fold desc="click on items Atoue">
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.i(TAG, "obser database: on item click " + atuolList.get(position).getTitle());

        for (int i = 0; i < addtoList.size(); i++) {
            if (atuolList.get(position).getTitle().equals(addtoList.get(i).getName()))
                return;
        }


        builder = new AlertDialog.Builder(CaloriesCalculatorActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        editcoleriBinding = DataBindingUtil.inflate(
                LayoutInflater.from(CaloriesCalculatorActivity.this), R.layout.observe_dialog_editcoleri, viewGroup, false);
        BaseObserverDialog baseDialog = new BaseObserverDialog();
        editcoleriBinding.setDialog(baseDialog);

        baseDialog.setName(atuolList.get(position).getTitle());
        baseDialog.setCalori(atuolList.get(position).getCalories());

        builder.setView(editcoleriBinding.getRoot());
        AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        editcoleriBinding.btnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (baseDialog.getMount() != null) {
                    double txtgrmedit = Double.valueOf((baseDialog.getMount()));
                    double calori = Double.valueOf(baseDialog.getCalori());
                    double newcoleri = ((calori * txtgrmedit) / 100);
                    Log.i(TAG, "newcoleri: " + newcoleri);

                    Material material = new Material();
                    material.setId(atuolList.get(position).getId());
                    material.setName(atuolList.get(position).getTitle());
                    material.setCalories(newcoleri);

                    addtoList.add(material);
                    baseCalori.setList(addtoList);
                    AddCalori();
                    binding.autoCompleteSpinner.setText("");

                    alertDialog.dismiss();

                } else {

                    baseDialog.setMount(100 + "");
                    double txtgrmedit = Double.valueOf(baseDialog.getMount());
                    double calori = Double.valueOf(baseDialog.getCalori());
                    double newcoleri = ((calori * txtgrmedit) / 100);
                    Log.i(TAG, "newcoleri: " + newcoleri);

                    Material material = new Material();
                    material.setId(atuolList.get(position).getId());
                    material.setName(atuolList.get(position).getTitle());
                    material.setCalories(newcoleri);
                    // material.setUnit(atuolList.get(position).getUnit());
                    addtoList.add(material);
                    baseCalori.setList(addtoList);
                    AddCalori();
                    binding.autoCompleteSpinner.setText("");
                    alertDialog.dismiss();
                }

            }
        });





    }
    //</editor-fold>

    //<editor-fold desc="click Back">
    public void btnBack() {
        onBackPressed();

    }
    //</editor-fold>

    //<editor-fold desc="lifcycle stop for delete All Data View ">
    @Override
    protected void onStop() {
        addtoList.clear();
        baseCalori.setList(addtoList);
        sumcalori = 0.0;
        baseCalori.setAddmount(sumcalori + "");
        super.onStop();
    }
    //</editor-fold>

    //<editor-fold desc="Click Next peper">
    public void btnNext() {

        if (addtoList.size() == 0) {
            Toast.makeText(CaloriesCalculatorActivity.this, "حداقل یک مواد غذایی انتخاب کنید", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(CaloriesCalculatorActivity.this, MealActivity.class);
        String data = "";
        for (int i = 0; i < addtoList.size(); i++) {
            data = data.concat(String.valueOf(addtoList.get(i).getId())).concat(",");
        }

        Log.i(TAG, "onClick: arr size is" + addtoList.size() + "data is :" + data);

        intent.putExtra("data", data);
        startActivity(intent);

    }

    //</editor-fold>



}
