package ir.adak.cookland.ui.caloriescalculator.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import ir.adak.cookland.R;
import ir.adak.cookland.pojos.MaterialDB;

public class CustomAdapter extends ArrayAdapter<MaterialDB> {

    private LayoutInflater layoutInflater;
    List<MaterialDB> mMaterials;

    private Filter mFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            return ((MaterialDB)resultValue).getTitle();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null) {
                ArrayList<MaterialDB> suggestions = new ArrayList<MaterialDB>();
                for (MaterialDB material : mMaterials) {
                    // Note: change the "contains" to "startsWith" if you only want starting matches
                    if (material.getTitle().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(material);
                    }
                }

                results.values = suggestions;
                results.count = suggestions.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            if (results != null && results.count > 0) {
                // we have filtered results
                addAll((ArrayList<MaterialDB>) results.values);
            } else {
                // no filter, add entire original list back in
                addAll(mMaterials);
            }
            notifyDataSetChanged();
        }
    };

    public CustomAdapter(Context context, List<MaterialDB> materials) {
        super(context, 0, materials);
        // copy all the materials into a master list
        mMaterials = new ArrayList<MaterialDB>(materials.size());
        mMaterials.addAll(materials);
        layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.row_auto_complate, null);
        }

        MaterialDB material = getItem(position);

        TextView name = (TextView) view.findViewById(R.id.customerNameLabel);
        name.setText(material.getTitle());

        return view;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }
}