package ir.adak.cookland.ui.recipe.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ir.adak.cookland.R;
import ir.adak.cookland.databinding.RowListGroceryRecipeBinding;
import ir.adak.cookland.listenerinteface.ClickInterface;
import ir.adak.cookland.pojos.FoodMaterial;
import ir.adak.cookland.pojos.ShoppingDB;

public class MaterialAdapetr extends RecyclerView.Adapter<MaterialAdapetr.ViewHolder> {
    private RowListGroceryRecipeBinding groceryRecipeBinding;
    private List<FoodMaterial> listRecipe;
    private List<ShoppingDB> shoppingDB;


    private ClickInterface.RecipeItemClick recipeItemClick;

    public MaterialAdapetr(ClickInterface.RecipeItemClick recipeItemClick) {
        this.recipeItemClick = recipeItemClick;
    }

    public void setListRecipe(List<FoodMaterial> listRecipe) {
        this.listRecipe = listRecipe;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        groceryRecipeBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_list_grocery_recipe, parent, false);
        return new ViewHolder(groceryRecipeBinding, recipeItemClick);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        FoodMaterial item = listRecipe.get(position);
        shoppingDB = new ArrayList<>();
        holder.nameRecipe.setText(item.getTitle());

//        if (shoppingDB.size() != 0) {
//            if (shoppingDB.get(position).getName().equals(item.getTitle()) && shoppingDB.get(position).getCheckValue()) {
//                holder.shoppingBtn.setBackgroundResource(R.drawable.do_shopping);
//            } else {
//                holder.shoppingBtn.setBackgroundResource(R.drawable.ic_shopping_cart_black_24dp);
//            }
//        }


    }

    @Override
    public int getItemCount() {
        return listRecipe.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView nameRecipe;
        private ImageButton shoppingBtn;
        private ClickInterface.RecipeItemClick recipeItemClick;


        public ViewHolder(RowListGroceryRecipeBinding groceryRecipeBinding, ClickInterface.RecipeItemClick recipeItemClick) {
            super(groceryRecipeBinding.getRoot());
            groceryRecipeBinding.executePendingBindings();
            this.recipeItemClick = recipeItemClick;
            nameRecipe = groceryRecipeBinding.nameRecipe;
            shoppingBtn = groceryRecipeBinding.shopBtn;

            shoppingBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    shoppingBtn.setBackgroundResource(R.drawable.do_shopping);
                    recipeItemClick.ItemRecipe(getAdapterPosition());

                }
            });
        }
    }
}
