package ir.adak.cookland.ui.home;

import android.app.Application;

import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ir.adak.cookland.pojos.ItemCategory;
import ir.adak.cookland.pojos.ItemFood;

public class CategoryViewModel extends AndroidViewModel {

    public CategoryRepository repository;
    public LiveData<List<ItemFood>> listSliderItem;
    public LiveData<List<ItemCategory>> listCategoryFood;

    public CategoryViewModel(@Nullable Application application) {
        super(application);
        repository = new CategoryRepository();
        listSliderItem = repository.getListSliderItem();
        listCategoryFood = repository.getListCategoryFood();


    }

    public LiveData<List<ItemCategory>> getListCategoryFood() {
        return listCategoryFood;
    }

    public LiveData<List<ItemFood>> getListSliderItem() {
        return listSliderItem;
    }



}
