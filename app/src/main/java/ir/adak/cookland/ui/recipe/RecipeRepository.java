package ir.adak.cookland.ui.recipe;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.util.List;

import ir.adak.cookland.network.RecipeRequestClass;
import ir.adak.cookland.pojos.ImagesCat;
import ir.adak.cookland.network.RetroClass;

public class RecipeRepository {

    public RecipeRequestClass recipeRequestClass;
    public MutableLiveData<List<ImagesCat>> listCategory;

    public RecipeRepository() {

        recipeRequestClass = RecipeRequestClass.getInstance();

        Log.e(RetroClass.TAG, "RecipeRepository: " + "request called");
        recipeRequestClass.getCategory();

        listCategory = recipeRequestClass.getListcategoryLiveData();
    }

    public MutableLiveData<List<ImagesCat>> getListCategory() {
        return listCategory;
    }
}
