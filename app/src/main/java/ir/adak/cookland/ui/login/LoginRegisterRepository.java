package ir.adak.cookland.ui.login;

import ir.adak.cookland.network.LoginRegisterRequestClass;

public class LoginRegisterRepository {
    private LoginRegisterRequestClass loginRegisterRequestClass;
    private String resultRegister;
    private int codeResponse;
    private String TokenUser;
    private String TokenAutologin;


    public LoginRegisterRepository() {
        loginRegisterRequestClass = LoginRegisterRequestClass.getInstance();

    }

    public void getLogin(String number) {
        loginRegisterRequestClass.loginUser(number);
        codeResponse = loginRegisterRequestClass.getCodeResponse();
    }

    public void getConfirm(String number, int codeVerify) {
        loginRegisterRequestClass.ConfirmUser(number,codeVerify);
    }

    public void getRecovery(String number) {
        loginRegisterRequestClass.recoveryCode(number);
        resultRegister = loginRegisterRequestClass.getResultResponse();
    }

    public void getAutoLogin(String number, String token) {
        loginRegisterRequestClass.loginAuto(number, token);
        TokenAutologin = loginRegisterRequestClass.getTokenAutoLogin();
    }

    public String getResultResponse() {
        return resultRegister;
    }




    public int getCodeResponse() {
        return codeResponse;
    }

    public String getTokenAutologin() {
        return TokenAutologin;
    }
}
