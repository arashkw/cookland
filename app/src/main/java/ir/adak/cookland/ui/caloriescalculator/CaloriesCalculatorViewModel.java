package ir.adak.cookland.ui.caloriescalculator;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ir.adak.cookland.pojos.MaterialDB;
import ir.adak.cookland.pojos.ShoppingDB;
import ir.adak.cookland.repository.CaloriesCalculatorRepository;
import ir.adak.cookland.repository.DBCaloriRepository;

public class CaloriesCalculatorViewModel extends AndroidViewModel {

    private CaloriesCalculatorRepository repository;
    private BaseObserverCalori baseCalori;

    private LiveData<List<MaterialDB>> liveDataRequest;
    private DBCaloriRepository dbCaloriRepository;
    private LiveData<List<MaterialDB>> liveDataDB;

    public CaloriesCalculatorViewModel(@NonNull Application application) {
        super(application);

        repository = CaloriesCalculatorRepository.getInstance();
        repository.getDataMatrial();
        liveDataRequest = repository.getMutableLiveData();

        dbCaloriRepository = new DBCaloriRepository(application);

        liveDataDB = dbCaloriRepository.getAllNote();

        baseCalori=new BaseObserverCalori();
    }

    public BaseObserverCalori getBaseCalori() {
        return baseCalori;
    }

    public LiveData<List<MaterialDB>> getLiveDataRequest() {
        return liveDataRequest;
    }

    public void insertDB(List<MaterialDB> materials) {
        dbCaloriRepository.insert(materials);
    }

    public void deleteDB() {
        dbCaloriRepository.delete();
    }

    public LiveData<List<MaterialDB>> getAllDB() {
        return liveDataDB;
    }


    public void insertRecipe(ShoppingDB shoppingDB) {
        dbCaloriRepository.insertRecipe(shoppingDB);
    }



}
