package ir.adak.cookland.ui.caloriescalculator;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;


public class BaseObserverDialog extends BaseObservable {

    //<editor-fold desc="Name's ِ item chosen for Dialog">
    private String name;
    //</editor-fold>

    //<editor-fold desc="Colori's Dialog">
    private String calori;
    //</editor-fold>

    //<editor-fold desc=" Mount Logged ٍ EditText's Dialog">
    private String mount;
    //</editor-fold>



    @Bindable
    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name=name;
        notifyPropertyChanged(BR.name);
    }

    @Bindable
    public String getCalori() {
        return calori;
    }

    public void setCalori(String calori) {
        this.calori = calori;
        notifyPropertyChanged(BR.calori);
    }

    @Bindable
    public String getMount() {
        return mount;
    }

    public void setMount(String mount) {
       this.mount = mount;
        notifyPropertyChanged(BR.mount);
    }




}
