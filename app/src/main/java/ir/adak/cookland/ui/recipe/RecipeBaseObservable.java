package ir.adak.cookland.ui.recipe;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;

import java.util.ArrayList;
import java.util.List;

import ir.adak.cookland.pojos.FoodMaterial;

public class RecipeBaseObservable extends BaseObservable {

    private List<FoodMaterial> foodMaterials;


    public RecipeBaseObservable() {
        foodMaterials = new ArrayList<>();
    }


    @Bindable
    public List<FoodMaterial> getFoodMaterials() {
        return foodMaterials;
    }

    public void setFoodMaterials(List<FoodMaterial> foodMaterials) {
        this.foodMaterials = foodMaterials;
        notifyPropertyChanged(BR.foodMaterials );
    }
}
