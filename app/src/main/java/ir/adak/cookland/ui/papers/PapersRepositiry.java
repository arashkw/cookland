package ir.adak.cookland.ui.papers;

import androidx.lifecycle.MutableLiveData;

import java.util.List;

import ir.adak.cookland.network.PaperRequestClass;
import ir.adak.cookland.pojos.ItemPaper;

public class PapersRepositiry {

    private PaperRequestClass paperRequestClass;
    private MutableLiveData<List<ItemPaper>> listMutableLiveData;

    public PapersRepositiry() {

        paperRequestClass=PaperRequestClass.getInstance();

        paperRequestClass.getPapers();

        listMutableLiveData=paperRequestClass.getListPaperLiveData();
    }

    public MutableLiveData<List<ItemPaper>> getListMutableLiveData() {
        return listMutableLiveData;
    }
}
