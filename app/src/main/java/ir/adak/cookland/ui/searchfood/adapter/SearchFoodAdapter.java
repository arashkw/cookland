package ir.adak.cookland.ui.searchfood.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import ir.adak.cookland.R;
import ir.adak.cookland.databinding.RowRecyclerVideoListBinding;
import ir.adak.cookland.listenerinteface.ClickInterface;
import ir.adak.cookland.pojos.ItemFood;
import ir.adak.cookland.pojos.ItemVideoList;

public class SearchFoodAdapter extends RecyclerView.Adapter<SearchFoodAdapter.ViewHolder> {

    private List<ItemFood> videoLists;
    private Context mContext;
    private RowRecyclerVideoListBinding videoListBinding;
    private ClickInterface.SerchFoodItemClick serchFoodItemClick;
    private boolean checkValue;

    public SearchFoodAdapter(ClickInterface.SerchFoodItemClick serchFoodItemClick ) {
        this.serchFoodItemClick=serchFoodItemClick;
    }

    public void setVideoLists(List<ItemFood> videoLists) {
        this.videoLists = videoLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        videoListBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_recycler_video_list, parent, false);
        return new ViewHolder(videoListBinding,serchFoodItemClick);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        ItemFood list = videoLists.get(position);

//        if (list.getDescription().length() > 10) {
//            String description = list.getDescription().substring(0, 10);
//            holder.descFood.setText(description + "...");
//        } else {
//            String description = list.getDescription();
//            holder.descFood.setText(description);
//        }
        holder.nameFood.setText(list.getTitle());
        Glide.with(holder.itemView)
                .load("http://adak-tech.ir/gwn/uploads/"+list.getImage())
                .centerInside()
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.progress_loader.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.progress_loader.setVisibility(View.GONE);
                        return false;
                    }
                })
                .apply(new RequestOptions().placeholder(R.drawable.ic_image_black_24dp).error(R.drawable.ic_image_black_24dp))
                .into(holder.imgFood);


    }

    @Override
    public int getItemCount() {
        return videoLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgFood;
        private TextView nameFood;
        private TextView descFood;
        private ImageButton favBtn;
        private ProgressBar progress_loader;
        private ClickInterface.SerchFoodItemClick serchFoodItemClick;

        public ViewHolder(@NonNull RowRecyclerVideoListBinding videoListBinding, ClickInterface.SerchFoodItemClick serchFoodItemClick) {
            super(videoListBinding.getRoot());
            videoListBinding.executePendingBindings();
            this.serchFoodItemClick=serchFoodItemClick;
            imgFood = videoListBinding.imageVideoList;
            nameFood = videoListBinding.nameFoodVideo;
            descFood = videoListBinding.descFoodVideo;
            favBtn = videoListBinding.favBtnFood;
            progress_loader=videoListBinding.progressLoader;


            videoListBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    serchFoodItemClick.itemFoodClick(getAdapterPosition());
                }
            });

            videoListBinding.favBtnFood.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (checkValue == false) {
                        checkValue = true;
                        videoListBinding.favBtnFood.setBackgroundResource(R.drawable.dofav);
                        Toast.makeText(view.getContext(), R.string.add_fav, Toast.LENGTH_SHORT).show();

                    } else {
                        checkValue = false;
                        videoListBinding.favBtnFood.setBackgroundResource(R.drawable.fav);
                        Toast.makeText(view.getContext(), R.string.del_fav, Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }


}
