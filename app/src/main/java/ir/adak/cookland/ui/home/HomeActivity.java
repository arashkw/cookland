package ir.adak.cookland.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dingmouren.layoutmanagergroup.skidright.SkidRightLayoutManager;

import java.util.ArrayList;
import java.util.List;

import ir.adak.cookland.R;
import ir.adak.cookland.databinding.ActivityMainBinding;
import ir.adak.cookland.listenerinteface.ClickInterface;
import ir.adak.cookland.network.RetroClass;
import ir.adak.cookland.pojos.ItemCategory;
import ir.adak.cookland.pojos.ItemFood;
import ir.adak.cookland.ui.caloriescalculator.CaloriesCalculatorActivity;
import ir.adak.cookland.ui.home.adapter.CategoryFoodItemAdapter;
import ir.adak.cookland.ui.home.adapter.SliderItemAdapter;
import ir.adak.cookland.ui.papers.PapersActivity;
import ir.adak.cookland.ui.profile.ProfileActivity;
import ir.adak.cookland.ui.recipe.RecipeActivity;
import ir.adak.cookland.ui.searchfood.SearchFoodActivity;
import ir.adak.cookland.ui.shoppinglist.ShoppingListActivity;


public class HomeActivity extends AppCompatActivity implements
        SliderItemAdapter.ClickSlider,
        ClickInterface.ClickOnMore, ClickInterface.ClickDetailsFood, View.OnClickListener {
    public ActivityMainBinding mainBinding;
    public CategoryViewModel viewModel;
    public List<ItemFood> itemSider = new ArrayList<>();
    public List<ItemCategory> itemCategoryListFood;
    public SliderItemAdapter sliderItemAdapter;
    private CategoryFoodItemAdapter foodItemAdapter;
    private SkidRightLayoutManager mSkidRightLayoutManager;
    private long back_pressed ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initUi();


        //<editor-fold desc="Set Sldider layout">
        sliderItemAdapter = new SliderItemAdapter(this, this);
        sliderItemAdapter.setItemSlider(new ArrayList<>());

        mSkidRightLayoutManager = new SkidRightLayoutManager(1.5f, 0.85f);
        mainBinding.recyclerSilder.setLayoutManager(mSkidRightLayoutManager);
        mainBinding.recyclerSilder.setAdapter(sliderItemAdapter);

        mainBinding.recyclerSilder.startAnimation(AnimationUtils.loadAnimation(this, R.anim.search_in_layout));

        mainBinding.recyclerSilder.showShimmerAdapter();

        new Handler().postDelayed((Runnable) () -> {

            mainBinding.recyclerSilder.hideShimmerAdapter();

        }, 3000);
        viewModel.getListSliderItem().observe(this, new Observer<List<ItemFood>>() {
            @Override
            public void onChanged(List<ItemFood> itemFoods) {
                sliderItemAdapter.setItemSlider(itemFoods);
                sliderItemAdapter.notifyDataSetChanged();
                itemSider = itemFoods;
                Log.e(RetroClass.TAG, "onChanged: Slider list size " + itemFoods.size());
            }
        });
        //</editor-fold>


        //<editor-fold desc="Recycler Category Food">
        foodItemAdapter = new CategoryFoodItemAdapter(this, this);
        foodItemAdapter.setCategoryFoods(new ArrayList<>());
        mainBinding.recyclerCategory.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        mainBinding.recyclerCategory.setAdapter(foodItemAdapter);
        mainBinding.recyclerCategory.setHasFixedSize(true);

        viewModel.getListCategoryFood().observe(this, new Observer<List<ItemCategory>>() {
            @Override
            public void onChanged(List<ItemCategory> itemCategoryFoods) {
                foodItemAdapter.setCategoryFoods(itemCategoryFoods);
                foodItemAdapter.notifyDataSetChanged();
                itemCategoryListFood = itemCategoryFoods;
            }
        });


        mainBinding.recyclerCategory.showShimmerAdapter();

        new Handler().postDelayed((Runnable) () -> {

            mainBinding.recyclerCategory.hideShimmerAdapter();

        }, 3000);


        //</editor-fold>


    }

    public void initUi() {
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        viewModel = ViewModelProviders.of(this).get(CategoryViewModel.class);
        mainBinding.layoutShopp.setOnClickListener(this);
        mainBinding.layoutDocument.setOnClickListener(this);
        mainBinding.layoutChef.setOnClickListener(this);
        mainBinding.profileBtn.setOnClickListener(this);


        Log.e(RetroClass.TAG, "initUi get token user : " + RetroClass.getToken());

    }


    @Override
    public void onMoreClick(int catId) {

        startActivity(new Intent(this, SearchFoodActivity.class)
                .putExtra("Cat_Id", itemCategoryListFood.get(catId).getId())
                .putExtra("Cat_Name", itemCategoryListFood.get(catId).getTitle()));

    }

    @Override
    public void onClickSlider(int position) {
        startActivity(new Intent(this, RecipeActivity.class)
                .putExtra("Item_Food", itemSider.get(position))
        );
    }


    @Override
    public void onClickDetails(int position, int v) {

        Intent intent = new Intent(this, RecipeActivity.class);
//        Toast.makeText(mContext, itemFoods.get(position), Toast.LENGTH_SHORT).show();
        Log.e(RetroClass.TAG, "onClickDetails: " + itemCategoryListFood.get(v).getItemFoodList().get(position));
        startActivity(intent.putExtra("Item_Food",
                itemCategoryListFood.get(v).getItemFoodList().get(position)));


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_shopp:
                startActivity(new Intent(this, ShoppingListActivity.class));
                break;
            case R.id.layout_document:
                startActivity(new Intent(this, PapersActivity.class));
                break;
            case R.id.layout_chef:
                startActivity(new Intent(this, CaloriesCalculatorActivity.class));
                break;
            case R.id.profile_btn:
                startActivity(new Intent(this, ProfileActivity.class));

                break;
            default:


        }
    }

    @Override
    public void onBackPressed() {

        if (back_pressed + 2000 > System.currentTimeMillis()) {
            super.onBackPressed();
            finishAffinity();
        } else {
            Toast.makeText(this, "یکبار دیگر کلیک کنید. ", Toast.LENGTH_SHORT).show();
            back_pressed = System.currentTimeMillis();

        }
    }
}

