package ir.adak.cookland.ui.caloriescalculator;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;

import java.util.ArrayList;
import java.util.List;

import ir.adak.cookland.pojos.Material;
import ir.adak.cookland.pojos.MaterialDB;

import static android.media.CamcorderProfile.get;

public class BaseObserverCalori extends BaseObservable {


    //<editor-fold desc="list Recrclerview ">
    public List<Material> list;
    //</editor-fold>


    //<editor-fold desc="sum's Colories">
    private String addmount;
    //</editor-fold>

    public BaseObserverCalori() {
        list =new ArrayList<>();
    }


    @Bindable
    public List<Material> getList() {
        return list;
    }

    public void setList(List<Material> list) {
        this.list = list;
        notifyPropertyChanged(BR.list);
    }


    @Bindable
    public String getAddmount() {
        return addmount;
    }

    public void setAddmount(String addmount) {
        this.addmount = addmount;
        notifyPropertyChanged(BR.addmount);
    }

}
