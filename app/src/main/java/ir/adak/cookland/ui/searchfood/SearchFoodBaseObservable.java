package ir.adak.cookland.ui.searchfood;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;

import java.util.ArrayList;
import java.util.List;

import ir.adak.cookland.pojos.ItemFood;

public class SearchFoodBaseObservable extends BaseObservable {


    private List<ItemFood> itemFoodList;

    public SearchFoodBaseObservable() {
      itemFoodList = new ArrayList<>();
    }

    @Bindable
    public List<ItemFood> getItemFoodList() {
        return itemFoodList;
    }

    public void setItemFoodList(List<ItemFood> itemFoodList) {
        this.itemFoodList = itemFoodList;
        notifyPropertyChanged(BR.itemFoodList);
    }
}
