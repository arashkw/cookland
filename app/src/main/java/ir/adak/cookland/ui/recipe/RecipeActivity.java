package ir.adak.cookland.ui.recipe;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;

import java.util.ArrayList;
import java.util.List;

import ir.adak.cookland.R;
import ir.adak.cookland.databinding.ActivityVideoListDetailsBinding;
import ir.adak.cookland.listenerinteface.ClickInterface;
import ir.adak.cookland.network.RetroClass;
import ir.adak.cookland.pojos.FoodMaterial;
import ir.adak.cookland.pojos.ImagesCat;
import ir.adak.cookland.pojos.ItemFood;
import ir.adak.cookland.pojos.ShoppingDB;
import ir.adak.cookland.repository.DBCaloriRepository;
import ir.adak.cookland.ui.recipe.adapter.MaterialAdapetr;
import ir.adak.cookland.ui.recipe.adapter.RecipeGalleryAdapter;

public class RecipeActivity extends AppCompatActivity implements ClickInterface.RecipeItemClick {
    private ActivityVideoListDetailsBinding detailsBinding;

    private RecipeGalleryAdapter adapter;
    private static RecipeViewModel recipeViewModel;
    private Bundle bundle;
    private ItemFood itemFood;
    private FoodMaterial foodMaterial;
    private List<ShoppingDB> shoppingDBList;
    private StaggeredGridLayoutManager staggeredGridLayoutManager;
    private SimpleExoPlayer exoPlayer;
    public int value = 1;
    private String path;
    private boolean checkValue;
    private boolean valueCheck;
    private static ShoppingDB shoppingDB;
    private static MaterialAdapetr materialAdapetr;
    private DBCaloriRepository dbCaloriRepository;
    private RecipeBaseObservable recipeBaseObservable;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();

        detailsBinding.favBtnDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkValue == true) {
                    checkValue = false;
                    detailsBinding.favBtnDetails.setBackgroundResource(R.drawable.fav);
                } else {
                    checkValue = true;
                    detailsBinding.favBtnDetails.setBackgroundResource(R.drawable.dofav);
                }
            }
        });
        detailsBinding.videoPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                detailsBinding.imageFood.setVisibility(View.GONE);
                detailsBinding.videoPlay.setVisibility(View.GONE);
                detailsBinding.exoPlayerView.setVisibility(View.VISIBLE);

                Log.e(RetroClass.TAG, "onClick path file:  " + path);
                getExo(path);
            }
        });


    }

    public void initUI() {
        detailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_video_list_details);
        recipeViewModel = ViewModelProviders.of(this).get(RecipeViewModel.class);

        recipeBaseObservable = recipeViewModel.getRecipeBaseObservable();
        detailsBinding.setMaterial(recipeBaseObservable);

        shoppingDB = new ShoppingDB();


//        materialAdapetr.setListRecipe(new ArrayList<>());
        bundle = getIntent().getExtras();
        if (bundle != null) {

            itemFood = bundle.getParcelable("Item_Food");
            if (itemFood != null) {

                detailsBinding.nameFoodDetails.setText(itemFood.getTitle());
                detailsBinding.timeCoocking.setText(itemFood.getTime());
                path = itemFood.getFilepath();
                Glide.with(getBaseContext())
                        .load("http://adak-tech.ir/gwn/uploads/" + itemFood.getImage())
                        .centerCrop()
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                return false;
                            }
                        })
                        .apply(new RequestOptions().placeholder(R.drawable.ic_image_black_24dp).error(R.drawable.ic_image_black_24dp))
                        .into(detailsBinding.imageFood);

                recipeBaseObservable.setFoodMaterials(itemFood.getFoodMaterials());


            } else if (itemFood == null) {
//                Log.e(RetroClass.TAG, "initUI Item Meal : " + itemMeal.getTitle());
                Glide.with(getBaseContext())
                        .load(R.drawable.backitem)
                        .centerCrop()
                        .into(detailsBinding.imageFood);

//                Log.e(RetroClass.TAG, "initUI Item Meal : " + itemMeal.getVideoname());

            }
        }

        detailsBinding.numberOfPerson.setText(String.valueOf(value));

        //<editor-fold desc="Gallery in Details Food">
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        adapter = new RecipeGalleryAdapter(this, new ArrayList<>());
        detailsBinding.recyclerGallery.setAdapter(adapter);
        detailsBinding.recyclerGallery.setLayoutManager(staggeredGridLayoutManager);

        recipeViewModel.getListLiveDataCategory().observe(this, new Observer<List<ImagesCat>>() {
            @Override
            public void onChanged(List<ImagesCat> itemCategories) {
                adapter.setItemCategoryList(itemCategories);
                adapter.notifyDataSetChanged();

            }
        });
        //</editor-fold>


        recipeViewModel.getListRecipeDatabase().observe(this, new Observer<List<ShoppingDB>>() {
            @Override
            public void onChanged(List<ShoppingDB> shoppingDBS) {
                shoppingDBList = shoppingDBS;
            }
        });
        calcul_person();
    }

    public void getExo(String videoURL) {
        try {

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
            exoPlayer = ExoPlayerFactory.newSimpleInstance(RecipeActivity.this, new DefaultTrackSelector());
            Uri videoURI = Uri.parse(videoURL);
            DefaultHttpDataSourceFactory dataSourceFactory = new DefaultHttpDataSourceFactory("exoplayer_video");
            ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
            MediaSource mediaSource = new ExtractorMediaSource(videoURI, dataSourceFactory, extractorsFactory, null, null);
            detailsBinding.exoPlayerView.setPlayer(exoPlayer);
            exoPlayer.prepare(mediaSource);
            exoPlayer.setPlayWhenReady(true);
        } catch (Exception e) {
            Log.e("MainAcvtivity", " exoplayer error " + e.toString());
        }
    }


    @Override
    protected void onStop() {
        if (exoPlayer != null) {
            exoPlayer.stop();
            exoPlayer.setPlayWhenReady(false);
            exoPlayer.release();
            exoPlayer = null;
        }
        super.onStop();
    }


    public void calcul_person() {
        detailsBinding.decrisePesron.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (value > 1) {
                    value--;
                    detailsBinding.numberOfPerson.setText(String.valueOf(value));
                } else return;
            }
        });

        detailsBinding.incrisePerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                value++;
                detailsBinding.numberOfPerson.setText(String.valueOf(value));

            }
        });

    }


    @BindingAdapter("bind:material")
    public static void getMaterial(RecyclerView recyclerView, List<FoodMaterial> materials) {
        materialAdapetr = new MaterialAdapetr(new ClickInterface.RecipeItemClick() {
            @Override
            public void ItemRecipe(int position) {

                if (!shoppingDB.getCheckValue()) {

                    shoppingDB.setName(materials.get(position).getTitle());
                    shoppingDB.setCheckValue(true);
                    recipeViewModel.getInsertRecipe(shoppingDB);
                    Toast.makeText(recyclerView.getContext(), "به لیست خرید اضافه شد.", Toast.LENGTH_SHORT).show();
                } else {
                    recipeViewModel.getDeleteItem(position);
                    Toast.makeText(recyclerView.getContext(), "از لیست خرید حذف شد.", Toast.LENGTH_SHORT).show();

                }
            }
        });

        materialAdapetr.setListRecipe(materials);
        recyclerView.setAdapter(materialAdapetr);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), LinearLayoutManager.VERTICAL, false));
        materialAdapetr.notifyDataSetChanged();
    }


    @Override
    public void ItemRecipe(int position) {

    }
}
