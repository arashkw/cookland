package ir.adak.cookland.ui.profile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import ir.adak.cookland.R;
import ir.adak.cookland.databinding.ActivityProfileBinding;
import ir.adak.cookland.databinding.CustomDialogProfileBmiBinding;
import ir.adak.cookland.databinding.CustomDialogProfileeditBinding;
import ir.adak.cookland.network.RetroClass;
import ir.adak.cookland.ui.home.HomeActivity;
import ir.adak.cookland.ui.profile.dialog.BaseObserveEditBmiDialog;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private ProfileViewModel viewModel;
    private ActivityProfileBinding binding;
    private BaseObserveEditBmiDialog baseobserBmi;
    private RetroClass retroClass;
    private Animation animation;
    private String name;
    private String family;
    private CustomDialogProfileBmiBinding customDialogBMiBinding;
    private CustomDialogProfileeditBinding customDialogProf;
    private final String TAG = "ProfileActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init();


        binding.backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileActivity.this, HomeActivity.class));
            }
        });


    }

    //<editor-fold desc="init">
    public void init() {
        viewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);
        animation = AnimationUtils.loadAnimation(this, R.anim.search_in_layout);
        binding.parentConstrain.startAnimation(animation);
        binding.myPage.setText(retroClass.getProfileSh()+"");
        retroClass = new RetroClass();
        binding.llEditProfile.setOnClickListener(this);
        binding.llBmi.setOnClickListener(this);
        baseobserBmi = viewModel.getBaseObserverBmi();
        binding.setBmi(baseobserBmi);


    }
    //</editor-fold>

    //<editor-fold desc="onclik">
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llBmi:
                showDialog(ProfileActivity.this, R.layout.custom_dialog_profile_bmi, 1);
                break;

            case R.id.llEditProfile:
                showDialog(ProfileActivity.this, R.layout.custom_dialog_profileedit, 2);
                break;

            case R.id.llFavourites:
                break;

            case R.id.llInvite:
                break;
        }
    }
    //</editor-fold>

    //<editor-fold desc="ShowDialog for Layots">
    public void showDialog(Context context, int layout, int cont) {

        if (cont == 1) {
            AlertDialog.Builder  builder = new AlertDialog.Builder(context);
            ViewGroup viewGroup = findViewById(android.R.id.content);
            customDialogBMiBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(context), layout, viewGroup, false);
            customDialogBMiBinding.setDialog(baseobserBmi);

            builder.setView(customDialogBMiBinding.getRoot());
            AlertDialog alertDialog = builder.create();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.show();

        } else if (cont == 2) {
            AlertDialog.Builder  builder = new AlertDialog.Builder(context);
            ViewGroup viewGroup = findViewById(android.R.id.content);
            customDialogProf = DataBindingUtil.inflate(
                    LayoutInflater.from(context), layout, viewGroup, false);


            builder.setView(customDialogProf.getRoot());
            AlertDialog alertDialog = builder.create();

            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.show();


            customDialogProf.btnProf.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(customDialogProf.txtName.length()>0 && customDialogProf.txtFamily.length()>0){

                        name=customDialogProf.txtName.getText().toString();
                        family=customDialogProf.txtFamily.getText().toString();

                        retroClass.SetProfileSh(name + " " + family);
                        binding.myPage.setText(retroClass.getProfileSh()+"");

                        String c=retroClass.getProfileSh();
                        Log.i(TAG, "onClick: "+c);

                        alertDialog.dismiss();


                    }else {
                        Toast.makeText(getApplicationContext(),"no exit",Toast.LENGTH_SHORT).show();
                    }


                }
            });


        }
    }


    //</editor-fold>
}
