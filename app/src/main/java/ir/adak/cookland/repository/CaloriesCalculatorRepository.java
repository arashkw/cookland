package ir.adak.cookland.repository;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import java.util.ArrayList;
import java.util.List;

import ir.adak.cookland.network.RequestInterface;
import ir.adak.cookland.network.RetroClass;
import ir.adak.cookland.pojos.CaloriCalculator;
import ir.adak.cookland.pojos.MaterialDB;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CaloriesCalculatorRepository {

    public static CaloriesCalculatorRepository instanse;
    private MutableLiveData<List<MaterialDB>> mutableLiveData;
    private List<MaterialDB> listmatrial;
    private final String TAG="CalorieRepository";



    private CaloriesCalculatorRepository() {
        mutableLiveData = new MutableLiveData<List<MaterialDB>>();
        listmatrial = new ArrayList<>();
    }


    public static CaloriesCalculatorRepository getInstance() {
        if (instanse == null)
            instanse = new CaloriesCalculatorRepository();
        return instanse;
    }


    public void getDataMatrial() {
        RequestInterface retrofitAPIs = RetroClass.getmRetrofit(RetroClass.Base_Url).create(RequestInterface.class);
        Call<CaloriCalculator> call = retrofitAPIs.getMatrial(RetroClass.Hash,"material");
        call.enqueue(new Callback<CaloriCalculator>() {
            @Override
            public void onResponse(Call<CaloriCalculator> call, Response<CaloriCalculator> response) {

                if (response.body() != null) {
                    if (response.body().getCode() == 200) {
                        listmatrial = response.body().getMaterial();
                        Log.i(TAG, "onResponse: "+response.raw().request().url());
                        Log.i(TAG, "getMaterial size: "+response.body().getMaterial().size());
                        mutableLiveData.setValue(listmatrial);
                    }
                }
            }
            @Override
            public void onFailure(Call<CaloriCalculator> call, Throwable t) {
                Log.i(TAG, "onFailure: "+t.getMessage());

            }
        });

    }


    public LiveData<List<MaterialDB>> getMutableLiveData() {
        return mutableLiveData;
    }



}
