package ir.adak.cookland.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import ir.adak.cookland.db.DBCaloriCalculator;
import ir.adak.cookland.db.DaoCaloriCalculator;
import ir.adak.cookland.pojos.MaterialDB;
import ir.adak.cookland.pojos.ShoppingDB;

public class DBCaloriRepository {

    private LiveData<List<MaterialDB>> getAll;
    public LiveData<List<ShoppingDB>> getRecipe;
    private DaoCaloriCalculator dao;

    public DBCaloriRepository(Application application) {
        DBCaloriCalculator db = DBCaloriCalculator.getInstance(application);
        dao = db.daoCalori();
        getAll = dao.getAllListAtuo();
        getRecipe = dao.getListRecepi();
    }

    public LiveData<List<MaterialDB>> getAllNote() {
        return getAll;
    }

    public void insert(List<MaterialDB> material) {
        new InsertAstncksTask(dao).execute(material);
    }


    public void delete() {
        new DeleteAstncksTask(dao).execute();
    }


    public LiveData<List<ShoppingDB>> getListRecipe() {
        return getRecipe;
    }

    public void insertRecipe(ShoppingDB shoppingDB) {
        new InsertRecipeTask(dao).execute(shoppingDB);
    }

    public void deleteRecipe(int id) {
        new DeleteRecipeTask(dao).execute(id);
    }


    private static class InsertAstncksTask extends AsyncTask<List<MaterialDB>, Void, Void> {


        private DaoCaloriCalculator dao;

        public InsertAstncksTask(DaoCaloriCalculator Dao) {
            this.dao = Dao;
        }

        @Override
        protected Void doInBackground(List<MaterialDB>... lists) {
            dao.insert(lists[0]);
            return null;
        }
    }

    private static class DeleteAstncksTask extends AsyncTask<Void, Void, Void> {

        private DaoCaloriCalculator dao;

        public DeleteAstncksTask(DaoCaloriCalculator Dao) {
            this.dao = Dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            dao.deleteAllNotes();
            return null;
        }
    }

    private static class DeleteRecipeTask extends AsyncTask<Integer,Void,Void>{
        private DaoCaloriCalculator dao;

        public DeleteRecipeTask(DaoCaloriCalculator Dao) {
        this.dao = Dao;
    }
        @Override
        protected Void doInBackground(Integer... id)
        {
            dao.deleteRecipe(id[0]);
            return null;
        }

    }

    private static class InsertRecipeTask extends AsyncTask<ShoppingDB, Void, Void> {
        private DaoCaloriCalculator dao;

        public InsertRecipeTask(DaoCaloriCalculator dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(ShoppingDB... shoppingDBS) {
            dao.insertRecipe(shoppingDBS[0]);
            return null;
        }
    }

}
