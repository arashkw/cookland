package ir.adak.cookland.repository;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import ir.adak.cookland.network.RequestInterface;
import ir.adak.cookland.network.RetroClass;
import ir.adak.cookland.pojos.ItemFood;
import ir.adak.cookland.pojos.ResFoodItem;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MealRepository {
    private static final String TAG = "MealRepository";
    private static MealRepository mealRepository;
    private MutableLiveData<List<ItemFood>> mutableLiveData;
    private List<ItemFood> meals;

    private MealRepository() {
        mutableLiveData = new MutableLiveData<>();
    }

    public static MealRepository getInstance() {
        if (mealRepository == null)
            mealRepository = new MealRepository();
        return mealRepository;

    }


    public void getfindFodd(String data) {
        RequestInterface apIs = RetroClass.getmRetrofit(RetroClass.Base_Url).create(RequestInterface.class);
        Call<ResFoodItem> call = apIs.getFindFoods(RetroClass.Hash, "suggest", data);
        call.enqueue(new Callback<ResFoodItem>() {
            @Override
            public void onResponse(Call<ResFoodItem> call, Response<ResFoodItem> response) {

                if (response.body().getmCode() == 200) {
                    Log.i(TAG, "onResponse Meal: " + response.raw().request().url());
                    meals = response.body().getListFoodItem();
                    mutableLiveData.setValue(meals);
                }

            }

            @Override
            public void onFailure(Call<ResFoodItem> call, Throwable t) {
                Log.i(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    public List<ItemFood> getMeals() {
        return meals;
    }

    public LiveData<List<ItemFood>> getMutableLiveData() {
        return mutableLiveData;
    }
}
