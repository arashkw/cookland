package ir.adak.cookland.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import ir.adak.cookland.pojos.ProfileInfoModel;

public class ProfileRepository {

    private LiveData<ProfileInfoModel> profileInfo;

    public ProfileRepository() {
        profileInfo = new MutableLiveData<>();
    }

    public void requestProfileInfo(){

    }

    public LiveData<ProfileInfoModel> getProfileInfo(){
        return profileInfo;
    }

    public void setProfileData(ProfileInfoModel profileData){

    }
}
