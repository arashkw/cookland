package ir.adak.cookland.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResVideoList {

    @SerializedName("code")
    private String code;

    @SerializedName("description")
    private String desc;

    @SerializedName("data")
    private List<ItemVideoList> data;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<ItemVideoList> getData() {
        return data;
    }

    public void setData(List<ItemVideoList> data) {
        this.data = data;
    }
}
