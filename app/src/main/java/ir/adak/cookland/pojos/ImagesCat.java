package ir.adak.cookland.pojos;

import com.google.gson.annotations.SerializedName;

public class ImagesCat {

    @SerializedName("id")
    private Integer id;
    @SerializedName("category_id")
    private Integer categoryId;
    @SerializedName("category")
    private String category;
    @SerializedName("title")
    private String title;
    @SerializedName("image")
    private String image;
    @SerializedName("time")
    private Object time;
    @SerializedName("description")
    private String description;
    @SerializedName("thumb")
    private String thumb;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Object getTime() {
        return time;
    }

    public void setTime(Object time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
}
