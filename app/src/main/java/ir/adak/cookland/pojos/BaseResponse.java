package ir.adak.cookland.pojos;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

public class BaseResponse {


    @SerializedName("code")
    private Integer code;
    @SerializedName("description")
    private String description;
    @SerializedName("data")
    private ResultData data;


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ResultData getData() {
        return data;
    }

    public void setData(ResultData data) {
        this.data = data;
    }
}
