package ir.adak.cookland.pojos;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "table_material")
public class MaterialDB {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "idDB")
    private int idDB;

    @SerializedName("id")
    private String id;


    @ColumnInfo(name = "title")
    @SerializedName("title")
    private String title;

    @ColumnInfo(name = "calories")
    @SerializedName("calories")
    private String Calories;

    @ColumnInfo(name = "unit")
    @SerializedName("unit")
    private String Unit;


    public int getIdDB() {
        return idDB;
    }

    public void setIdDB(int idDB) {
        this.idDB = idDB;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCalories() {
        return Calories;
    }

    public void setCalories(String calories) {
        Calories = calories;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }
}


