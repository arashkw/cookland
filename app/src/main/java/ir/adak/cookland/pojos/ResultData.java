package ir.adak.cookland.pojos;

import com.google.gson.annotations.SerializedName;

public class ResultData {

    @SerializedName("success")
    private String success;
    @SerializedName("error")
    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
