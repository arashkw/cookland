package ir.adak.cookland.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Res_Category {

    @SerializedName("code")
    private Integer mCode;

    @SerializedName("description")
    private String mDes;
    @SerializedName("data")
    private List<ItemCategory> mData;


    public Integer getmCode() {
        return mCode;
    }

    public void setmCode(Integer mCode) {
        this.mCode = mCode;
    }

    public String getmDes() {
        return mDes;
    }

    public void setmDes(String mDes) {
        this.mDes = mDes;
    }

    public List<ItemCategory> getmData() {
        return mData;
    }

    public void setmData(List<ItemCategory> mData) {
        this.mData = mData;
    }
}
