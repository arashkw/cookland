package ir.adak.cookland.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ItemFood implements Parcelable {

    @SerializedName("id")
    private String id;

    @SerializedName("app_id")
    private String appId;

    @SerializedName("category_id")
    private String categoryId;

    @SerializedName("title")
    private String title;

    @SerializedName("image")
    private String image;

    @SerializedName("time")
    private String time;

    @SerializedName("appetizer")
    private String appetizer;

    @SerializedName("food")
    private String food;

    @SerializedName("breakfast")
    private String breakfast;

    @SerializedName("filepath")
    private String filepath;

    @SerializedName("home_page")
    private String homePage;

    @SerializedName("foodMaterials")
    private List<FoodMaterial> foodMaterials;


    private int existInCatCount = 0;

    protected ItemFood(Parcel in) {
        id = in.readString();
        appId = in.readString();
        categoryId = in.readString();
        title = in.readString();
        image = in.readString();
        time = in.readString();
        appetizer = in.readString();
        food = in.readString();
        breakfast = in.readString();
        filepath = in.readString();
        homePage = in.readString();
        foodMaterials = in.createTypedArrayList(FoodMaterial.CREATOR);
        existInCatCount = in.readInt();
    }

    public static final Creator<ItemFood> CREATOR = new Creator<ItemFood>() {
        @Override
        public ItemFood createFromParcel(Parcel in) {
            return new ItemFood(in);
        }

        @Override
        public ItemFood[] newArray(int size) {
            return new ItemFood[size];
        }
    };

    public int getExistInCatCount() {
        return existInCatCount;
    }

    public void setExistInCatCount(int existInCatCount) {
        if (existInCatCount >= 0 && existInCatCount < 4) {
            this.existInCatCount = existInCatCount;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAppetizer() {
        return appetizer;
    }

    public void setAppetizer(String appetizer) {
        this.appetizer = appetizer;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getBreakfast() {
        return breakfast;
    }

    public void setBreakfast(String breakfast) {
        this.breakfast = breakfast;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public String getHomePage() {
        return homePage;
    }

    public void setHomePage(String homePage) {
        this.homePage = homePage;
    }

    public List<FoodMaterial> getFoodMaterials() {
        return foodMaterials;
    }

    public void setFoodMaterials(List<FoodMaterial> foodMaterials) {
        this.foodMaterials = foodMaterials;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(appId);
        dest.writeString(categoryId);
        dest.writeString(title);
        dest.writeString(image);
        dest.writeString(time);
        dest.writeString(appetizer);
        dest.writeString(food);
        dest.writeString(breakfast);
        dest.writeString(filepath);
        dest.writeString(homePage);
        dest.writeTypedList(foodMaterials);
        dest.writeInt(existInCatCount);
    }
}