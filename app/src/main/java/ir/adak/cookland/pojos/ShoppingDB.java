package ir.adak.cookland.pojos;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity(tableName = "table_shop")
public class ShoppingDB {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "name")
    private String Name;

    @ColumnInfo(name = "check")
    private boolean CheckValue;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public boolean getCheckValue() {
        return CheckValue;
    }

    public void setCheckValue(boolean checkValue) {
        CheckValue = checkValue;
    }
}
