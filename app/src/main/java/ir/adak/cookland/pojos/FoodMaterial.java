package ir.adak.cookland.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class FoodMaterial implements Parcelable{

    @SerializedName("id")
    private String id;

    @SerializedName("food_id")
    private String foodId;

    @SerializedName("material_id")
    private String materialId;

    @SerializedName("title")
    private String title;

    @SerializedName("calories")
    private String calories;

    @SerializedName("unit")
    private String unit;


    protected FoodMaterial(Parcel in) {
        id = in.readString();
        foodId = in.readString();
        materialId = in.readString();
        title = in.readString();
        calories = in.readString();
        unit = in.readString();
    }

    public static final Creator<FoodMaterial> CREATOR = new Creator<FoodMaterial>() {
        @Override
        public FoodMaterial createFromParcel(Parcel in) {
            return new FoodMaterial(in);
        }

        @Override
        public FoodMaterial[] newArray(int size) {
            return new FoodMaterial[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFoodId() {
        return foodId;
    }

    public void setFoodId(String foodId) {
        this.foodId = foodId;
    }

    public String getMaterialId() {
        return materialId;
    }

    public void setMaterialId(String materialId) {
        this.materialId = materialId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCalories() {
        return calories;
    }

    public void setCalories(String calories) {
        this.calories = calories;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(foodId);
        parcel.writeString(materialId);
        parcel.writeString(title);
        parcel.writeString(calories);
        parcel.writeString(unit);
    }
}
