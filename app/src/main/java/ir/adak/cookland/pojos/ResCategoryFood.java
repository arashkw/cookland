package ir.adak.cookland.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResCategoryFood {


    @SerializedName("code")
    private Integer code;

    @SerializedName("description")
    private String description;

    @SerializedName("data")
    private List<ItemCategoryFood> data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ItemCategoryFood> getData() {
        return data;
    }

    public void setData(List<ItemCategoryFood> data) {
        this.data = data;
    }
}
