package ir.adak.cookland.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResSearchFood {

    @SerializedName("code")
    private int mCode;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("data")
    private List<ItemFood> mResult;

    public int getmCode() {
        return mCode;
    }

    public void setmCode(int mCode) {
        this.mCode = mCode;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }


    public List<ItemFood> getmResult() {
        return mResult;
    }

    public void setmResult(List<ItemFood> mResult) {
        this.mResult = mResult;
    }
}
