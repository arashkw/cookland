package ir.adak.cookland.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResFoodItem {

    @SerializedName("code")
    private Integer mCode;

    @SerializedName("description")
    private String mDes;
    @SerializedName("data")
    private List<ItemFood> listFoodItem;


    public Integer getmCode() {
        return mCode;
    }

    public void setmCode(Integer mCode) {
        this.mCode = mCode;
    }

    public String getmDes() {
        return mDes;
    }

    public void setmDes(String mDes) {
        this.mDes = mDes;
    }

    public List<ItemFood> getListFoodItem() {
        return listFoodItem;
    }

    public void setListFoodItem(List<ItemFood> listFoodItem) {
        this.listFoodItem = listFoodItem;
    }
}
