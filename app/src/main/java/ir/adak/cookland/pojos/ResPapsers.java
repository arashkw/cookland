package ir.adak.cookland.pojos;

import com.google.gson.annotations.SerializedName;

import java.io.DataInputStream;
import java.util.List;

public class ResPapsers {

    @SerializedName("code")
    private Integer code;
    @SerializedName("description")
    private String description;
    @SerializedName("data")
    private List<ItemPaper> data ;


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ItemPaper> getData() {
        return data;
    }

    public void setData(List<ItemPaper> data) {
        this.data = data;
    }
}
