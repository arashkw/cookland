package ir.adak.cookland.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResImages {

    @SerializedName("code")
    private Integer code;
    @SerializedName("description")
    private String description;
    @SerializedName("data")
    private List<ImagesCat> mData;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ImagesCat> getmData() {
        return mData;
    }

    public void setmData(List<ImagesCat> mData) {
        this.mData = mData;
    }
}
