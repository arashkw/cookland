package ir.adak.cookland.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ItemCategory {

    @SerializedName("id")
    private String id;

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("image")
    private String image;

    @SerializedName("count")
    private String count;

    @SerializedName("video_list")
    private List<ItemFood> itemFoodList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public List<ItemFood> getItemFoodList() {
        return itemFoodList;
    }

    public void setItemFoodList(List<ItemFood> itemFoodList) {
        this.itemFoodList = itemFoodList;
    }
}
