package ir.adak.cookland.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CaloriCalculator {

    @SerializedName("code")
    private int Code;

    @SerializedName("description")
    private String description;

    @SerializedName("data")
    private List<MaterialDB> material;


    public int getCode() {
        return Code;
    }

    public void setCode(int code) {
        Code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<MaterialDB> getMaterial() {
        return material;
    }

    public void setMaterial(List<MaterialDB> material) {
        this.material = material;
    }
}
